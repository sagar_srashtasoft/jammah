package com.jammaah.net.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.github.fobid.linkabletext.view.OnLinkClickListener;
import com.github.fobid.linkabletext.widget.LinkableTextView;
import com.jammaah.net.model.Friend;
import com.jammaah.net.util.CustomRequest;
import com.pkmmte.view.CircularImageView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import github.ankushsachdeva.emojicon.EmojiconTextView;
import com.jammaah.net.ProfileActivity;
import com.jammaah.net.R;
import com.jammaah.net.app.App;
import com.jammaah.net.constants.Constants;
import com.jammaah.net.model.Comment;
import com.jammaah.net.util.CommentInterface;
import com.jammaah.net.util.TagSelectingTextview;
import com.jammaah.net.view.ResizableImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class CommentListAdapter extends BaseAdapter implements Constants {

	private Activity activity;
	private LayoutInflater inflater;
	private List<Comment> commentsList;
    //using for Tag Friend
    List<Friend> friends=new ArrayList<>();
    HashMap<Long,String> friendlist=new HashMap<>();
    private static Pattern MENTION_PATTERN;
    private Long profileId;
    long itemId = 0;
    int arrayLength = 0;
    private int friendItemId = 0;
    private CommentInterface responder;

    private Boolean myPost = false;

    TagSelectingTextview mTagSelectingTextview;

    public static int hashTagHyperLinkEnabled = 1;
    public static int hashTagHyperLinkDisabled = 0;

    ImageLoader imageLoader = App.getInstance().getImageLoader();

	public CommentListAdapter(Activity activity, List<Comment> commentsList, CommentInterface responder) {

		this.activity = activity;
		this.commentsList = commentsList;
        this.responder = responder;

        mTagSelectingTextview = new TagSelectingTextview();

        getFriendList();
	}

    public void setMyPost(Boolean myPost) {

        this.myPost = myPost;
    }

    public Boolean getMyPost() {

        return this.myPost;
    }

	@Override
	public int getCount() {

		return commentsList.size();
	}

	@Override
	public Object getItem(int location) {

		return commentsList.get(location);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}



    static class ViewHolder {

        public CircularImageView commentAuthorPhoto;
        public TextView commentAuthor;
        public LinkableTextView commentText;
        public TextView commentTimeAgo;
        public TextView commentLikesCount;
        public ImageView commentLike;
        public ImageView commentAction;
        public ResizableImageView commentImg;
	        
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		ViewHolder viewHolder = null;
        String mentionPattern = null;
		if (inflater == null) {

            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

		if (convertView == null) {
			
			convertView = inflater.inflate(R.layout.comment_list_row, null);
			
			viewHolder = new ViewHolder();

            viewHolder.commentAuthorPhoto = (CircularImageView) convertView.findViewById(R.id.commentAuthorPhoto);
            viewHolder.commentImg = (ResizableImageView) convertView.findViewById(R.id.commentImg);
            viewHolder.commentLike = (ImageView) convertView.findViewById(R.id.commentLike);
            viewHolder.commentAction = (ImageView) convertView.findViewById(R.id.commentAction);
            viewHolder.commentLikesCount = (TextView) convertView.findViewById(R.id.commentLikesCount);
			viewHolder.commentText = (LinkableTextView) convertView.findViewById(R.id.commentText);

            viewHolder.commentAuthor = (TextView) convertView.findViewById(R.id.commentAuthor);
            viewHolder.commentTimeAgo = (TextView) convertView.findViewById(R.id.commentTimeAgo);

//            viewHolder.questionRemove.setTag(position);
            convertView.setTag(viewHolder);

		} else {
			
			viewHolder = (ViewHolder) convertView.getTag();
		}

        if (imageLoader == null) {

            imageLoader = App.getInstance().getImageLoader();
        }

        viewHolder.commentAuthorPhoto.setTag(position);
        viewHolder.commentText.setTag(position);
        viewHolder.commentAuthor.setTag(position);
        viewHolder.commentTimeAgo.setTag(position);
        viewHolder.commentImg.setTag(position);
        viewHolder.commentAction.setTag(position);
        viewHolder.commentLikesCount.setTag(position);
        viewHolder.commentLike.setTag(position);
		
		final Comment comment = commentsList.get(position);

        viewHolder.commentAuthor.setVisibility(View.VISIBLE);
        viewHolder.commentAuthor.setText(comment.getFromUserFullname());

        if (comment.getFromUserVerify() != 1) {

            viewHolder.commentAuthor.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

        } else {

            viewHolder.commentAuthor.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.profile_verify_icon, 0);
        }

        viewHolder.commentAuthor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, ProfileActivity.class);
                intent.putExtra("profileId", comment.getFromUserId());
                activity.startActivity(intent);
            }
        });

        if (comment.getFromUserPhotoUrl().length() != 0) {

            viewHolder.commentAuthorPhoto.setVisibility(View.VISIBLE);

            imageLoader.get(comment.getFromUserPhotoUrl(), ImageLoader.getImageListener(viewHolder.commentAuthorPhoto, R.drawable.profile_default_photo, R.drawable.profile_default_photo));

        } else {

            viewHolder.commentAuthorPhoto.setVisibility(View.VISIBLE);
            viewHolder.commentAuthorPhoto.setImageResource(R.drawable.profile_default_photo);
        }

        viewHolder.commentAuthorPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, ProfileActivity.class);
                intent.putExtra("profileId", comment.getFromUserId());
                activity.startActivity(intent);
            }
        });

        viewHolder.commentAction.setVisibility(View.VISIBLE);

        viewHolder.commentAction.setImageResource(R.drawable.ic_action_collapse);

        viewHolder.commentAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final int getPosition = (Integer) view.getTag();

                responder.commentAction(getPosition);
            }
        });

        viewHolder.commentText.setText(comment.getText().replaceAll("<br>", "\n"));

        viewHolder.commentText.setMovementMethod(LinkMovementMethod.getInstance());

        String textHtml = comment.getText();

       /* for(Friend friend:friends)
        {
            if(textHtml.toLowerCase().contains(friend.getFriendUserUsername().toLowerCase()))
            {

            }
        }*/
        viewHolder.commentText.setText(textHtml);
       viewHolder.commentText.setOnLinkClickListener(new OnLinkClickListener() {
           @Override
           public void onHashtagClick(String hashtag) {

           }

           @Override
           public void onMentionClick(String mention) {
              // Toast.makeText(activity, mention, Toast.LENGTH_SHORT).show();
               profileId=comment.getFromUserId();

               getFriendList();

               for(Map.Entry list:friendlist.entrySet())
               {
                   if(list.getValue().toString().equalsIgnoreCase(mention)){
                       Intent intent = new Intent(activity, ProfileActivity.class);
                       intent.putExtra("profileId", Long.valueOf(list.getKey().toString()));
                       activity.startActivity(intent);
                       break;
                   }

               }


           }

           @Override
           public void onEmailAddressClick(String email) {

           }

           @Override
           public void onWebUrlClick(String url) {

           }

           @Override
           public void onPhoneClick(String phone) {

           }
       });

        viewHolder.commentText.setVisibility(View.VISIBLE);

        String timeAgo;

        timeAgo = comment.getTimeAgo();

        if (comment.getReplyToUserId() != 0) {

            if (comment.getReplyToUserFullname().length() != 0) {

                timeAgo = timeAgo + " " + activity.getString(R.string.label_to) + " " + comment.getReplyToUserFullname();

            } else {

                timeAgo = timeAgo + " " + activity.getString(R.string.label_to) + " @" + comment.getReplyToUserUsername();
            }
        }

        viewHolder.commentTimeAgo.setVisibility(View.VISIBLE);
        viewHolder.commentTimeAgo.setText(timeAgo);

        viewHolder.commentImg.setVisibility(View.GONE);

        viewHolder.commentLike.setVisibility(View.GONE);
        viewHolder.commentLikesCount.setVisibility(View.GONE);
        viewHolder.commentLike.setImageResource(R.drawable.perk);

		return convertView;
	}
    public void getFriendList() {
        CustomRequest jsonReq = new CustomRequest(Request.Method.POST, METHOD_FRIENDS_GET, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        if ( activity == null) {

                            Log.e("ERROR", "Friends Fragment Not Added to Activity");

                            return;
                        }

                        try {

                            arrayLength = 0;
                            friends.clear();
                            if (!response.getBoolean("error")) {

                                if (itemId == 0) {

                                    // App.getInstance().setNewFriendsCount(0);
                                }

                                friendItemId = response.getInt("itemId");

                                if (response.has("items")) {

                                    JSONArray usersArray = response.getJSONArray("items");

                                    arrayLength = usersArray.length();

                                    if (arrayLength > 0) {

                                        for (int i = 0; i < usersArray.length(); i++) {

                                            JSONObject userObj = (JSONObject) usersArray.get(i);

                                            Friend item = new Friend(userObj);
                                            friendlist.put(item.getFriendUserId(),item.getFriendUserUsername());
                                            friends.add(item);

                                        }
                                        Log.d("FriendList=",friends.toString());
                                    }
                                }

                            }

                        } catch (JSONException e) {

                            e.printStackTrace();

                        } finally {

                            Log.d("Friends", response.toString());


                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (activity == null) {

                    Log.e("ERROR", "Friends Fragment Not Added to Activity");

                    return;
                }


            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("accountId", Long.toString(App.getInstance().getId()));
                params.put("accessToken", App.getInstance().getAccessToken());
                params.put("profileId", Long.toString(App.getInstance().getId()));
                params.put("itemId", Long.toString(0));
                params.put("language", "en");


                return params;
            }
        };

        App.getInstance().addToRequestQueue(jsonReq);


    }


}