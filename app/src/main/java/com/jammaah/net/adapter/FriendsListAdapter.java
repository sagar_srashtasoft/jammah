package com.jammaah.net.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;
import com.jammaah.net.app.App;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import com.jammaah.net.R;
import com.jammaah.net.model.Friend;


public class FriendsListAdapter extends RecyclerView.Adapter<FriendsListAdapter.MyViewHolder> {

	private Context mContext;
	private List<Friend> itemList;
    public tapToRefresh mtapToRefresh;

	public class MyViewHolder extends RecyclerView.ViewHolder {

		public TextView mProfileFullname, mProfileUsername;
		public ImageView mProfilePhoto, mProfileOnlineIcon, mProfileIcon;
		public MaterialRippleLayout mParent;
		public ProgressBar mProgressBar;
        public LinearLayout ll_no_internet;

		public MyViewHolder(View view,int Itemtype) {

			super(view);
            if(Itemtype == 0) {

                mParent = (MaterialRippleLayout) view.findViewById(R.id.parent);

                mProfilePhoto = (ImageView) view.findViewById(R.id.profileImg);
                mProfileFullname = (TextView) view.findViewById(R.id.profileFullname);
                mProfileUsername = (TextView) view.findViewById(R.id.profileUsername);
                mProfileOnlineIcon = (ImageView) view.findViewById(R.id.profileOnlineIcon);
                mProfileIcon = (ImageView) view.findViewById(R.id.profileIcon);
                mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            }else{
                ll_no_internet = (LinearLayout)view.findViewById(R.id.ll_no_internet);
            }
		}
	}


	public FriendsListAdapter(Context mContext, List<Friend> itemList) {

		this.mContext = mContext;
		this.itemList = itemList;
	}

    @Override
    public int getItemViewType(int position) {
       final Friend f = itemList.get(position);
        if(f.getId() == 0){
            return 1;
        }else{
            return 0;
        }

      //  return super.getItemViewType(position);
    }

    public void add_noInternertTab(Friend friend){
        itemList.add(friend);
        notifyItemInserted(itemList.size());
    }

    @Override
	public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if(viewType == 0) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_thumbnail, parent, false);


            return new MyViewHolder(itemView,viewType);
        }else{
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.tab_pull_refresh, parent, false);


            return new MyViewHolder(itemView,viewType);
        }
	}

	@Override
	public void onBindViewHolder(final MyViewHolder holder, final int position) {

		Friend item = itemList.get(position);

        if(item.getId() == 0){
            holder.ll_no_internet.setVisibility(View.VISIBLE);
            holder.ll_no_internet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (App.getInstance().isConnected()) {
                        if(mtapToRefresh != null) {
                            mtapToRefresh.OnTaptoRefresh();
                            itemList.remove(position);
                            notifyItemRemoved(position);
                        }

                    }else{
                        Toast.makeText(mContext,"No Internet Connection",Toast.LENGTH_LONG).show();
                    }
                }
            });
        }else {

            holder.mProgressBar.setVisibility(View.VISIBLE);
            holder.mProfilePhoto.setVisibility(View.GONE);

            if (item.getFriendUserPhotoUrl() != null && item.getFriendUserPhotoUrl().length() > 0) {

                final ImageView img = holder.mProfilePhoto;
                final ProgressBar progressView = holder.mProgressBar;

                Picasso.with(mContext)
                        .load(item.getFriendUserPhotoUrl())
                        .into(holder.mProfilePhoto, new Callback() {

                            @Override
                            public void onSuccess() {

                                progressView.setVisibility(View.GONE);
                                img.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError() {

                                progressView.setVisibility(View.GONE);
                                img.setVisibility(View.VISIBLE);
                                img.setImageResource(R.drawable.profile_default_photo);
                            }
                        });

            } else {

                holder.mProgressBar.setVisibility(View.GONE);
                holder.mProfilePhoto.setVisibility(View.VISIBLE);

                holder.mProfilePhoto.setImageResource(R.drawable.profile_default_photo);
            }

            holder.mProfileFullname.setText(item.getFriendUserFullname());

            holder.mProfileUsername.setText("@" + item.getFriendUserUsername());

            if (item.isOnline()) {

                holder.mProfileOnlineIcon.setVisibility(View.VISIBLE);

            } else {

                holder.mProfileOnlineIcon.setVisibility(View.GONE);
            }

            if (item.isVerify()) {

                holder.mProfileIcon.setVisibility(View.VISIBLE);

            } else {

                holder.mProfileIcon.setVisibility(View.GONE);
            }
        }
	}

	@Override
	public int getItemCount() {

		return itemList.size();
	}

    public interface tapToRefresh{
        void OnTaptoRefresh();
    }
}