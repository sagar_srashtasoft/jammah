package com.jammaah.net.adapter;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.jammaah.net.model.Profile;

import java.util.ArrayList;
import java.util.List;

//create Sql lite data to help Search by name,location and familyname
public class AutoCompleteDbAdapter {
       ArrayList<Profile> mList=new ArrayList<>();
    private static final String DATABASE_NAME = "search";
    private static final String TABLE_NAME = "profile";
   // private static final int DATABASE_VERSION = 2;
    private static final String[] fullname = {"hussain","hamid","abc","abd","bbc","sayad","xyz","zafir","zakir"};
    private static final String[] location = {"london","paris","debai","USA","delhi","mysore","new york","nevada","china"};
    private static final String[] familyname = {"bal","yusuf","xyz","abd","bbc","sayad","xyz","zafir","husain"};

    private  int DATABASE_VERSION;

    private class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            final String DATABASE_CREATE_SEARCH =
                    "create table " + TABLE_NAME
                            + "(_id integer primary key autoincrement"
                            + ", fullname text not null"
                            + ", location text not null"
                            + ", familyname text not null)";

            db.execSQL(DATABASE_CREATE_SEARCH);
            populateWithData(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
        }

        @Override
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
        }
    }

    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;
    private final Activity mActivity;


    public AutoCompleteDbAdapter(Activity activity, ArrayList<Profile> mList,int dbnewversion) {
        this.mActivity = activity;
        this.mList=mList;
        this.DATABASE_VERSION=dbnewversion;
        mDbHelper = this.new DatabaseHelper(activity);
        mDb = mDbHelper.getWritableDatabase();
    }

    /**
     * Closes the database.
     */
  /*  public void close()
    {
        mDbHelper.close();
    }*/

    public Cursor getMatchingLocation(String constraint) throws SQLException {

        String queryString =
                "SELECT _id, fullname, location, familyname FROM " + TABLE_NAME;

        if (constraint != null) {

            constraint = constraint.trim() + "%";
            queryString += " WHERE location LIKE ?";
        }
        String params[] = { constraint };

        if (constraint == null) {
            // If no parameters are used in the query,
            // the params arg must be null.
            params = null;
        }
        try {
            Cursor cursor = mDb.rawQuery(queryString, params);
            if (cursor != null) {
                this.mActivity.startManagingCursor(cursor);
                cursor.moveToFirst();
                this.mActivity.stopManagingCursor(cursor);
                mDbHelper.close();
                return cursor;
            }

        }
        catch (SQLException e) {
            Log.e("AutoCompleteDbAdapter", e.toString());
            throw e;
        }

        return null;
    }

    public Cursor getMatchingFullname(String constraint) throws SQLException {

        String queryString =
                "SELECT _id, fullname, location, familyname FROM " + TABLE_NAME;

        if (constraint != null) {

            constraint = constraint.trim() + "%";
            queryString += " WHERE fullname LIKE ?";
        }
        String params[] = { constraint };

        if (constraint == null) {
            // If no parameters are used in the query,
            // the params arg must be null.
            params = null;
        }
        try {
            Cursor cursor = mDb.rawQuery(queryString, params);
            if (cursor != null) {
                this.mActivity.startManagingCursor(cursor);
                cursor.moveToFirst();
                this.mActivity.stopManagingCursor(cursor);
                mDbHelper.close();
                return cursor;
            }

        }
        catch (SQLException e) {
            Log.e("AutoCompleteDbAdapter", e.toString());
            throw e;
        }

        return null;
    }

    public Cursor getMatchingFname(String constraint) throws SQLException {

        String queryString =
                "SELECT _id, fullname, location, familyname FROM " + TABLE_NAME;

        if (constraint != null) {

            constraint = constraint.trim() + "%";
            queryString += " WHERE familyname LIKE ?";
        }
        String params[] = { constraint };

        if (constraint == null) {
            // If no parameters are used in the query,
            // the params arg must be null.
            params = null;
        }
        try {
            Cursor cursor = mDb.rawQuery(queryString, params);
            if (cursor != null) {
                this.mActivity.startManagingCursor(cursor);
                cursor.moveToFirst();
                this.mActivity.stopManagingCursor(cursor);
                mDbHelper.close();
                return cursor;
            }

        }
        catch (SQLException e) {
            Log.e("AutoCompleteDbAdapter", e.toString());
            throw e;
        }

        return null;
    }



    private void populateWithData(SQLiteDatabase db) {
        try {
            db.beginTransaction();
            ContentValues values = new ContentValues();

            Log.d("SqlData",mList+"");
            for(Profile list:mList)
            {
                values.put("fullname",list.getFullname());
                values.put("location",list.getLocation());
                values.put("familyname",list.getFamilyname());
                db.insert(TABLE_NAME, "fullname", values);
            }
          /*  for (int i=0;i<fullname.length;i++) {
                values.put("fullname", fullname[i]);
                values.put("location",location[i]);
                values.put("familyname",familyname[i]);

                db.insert(TABLE_NAME, "fullname", values);
            }*/
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
        }
    }

}
