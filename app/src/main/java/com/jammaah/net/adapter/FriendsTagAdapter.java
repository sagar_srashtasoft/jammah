package com.jammaah.net.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;
import com.jammaah.net.R;
import com.jammaah.net.model.Friend;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by SAGAR on 23/05/2017.
 */
public class FriendsTagAdapter extends RecyclerView.Adapter<FriendsTagAdapter.MyViewHolder> implements Filterable {

    private Context mContext;
    private List<Friend> friendList=new ArrayList<>();
    private List<Friend> filterfriendList=new ArrayList<>();
    private UserFilter userFilter;
    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView friendName;


        public MyViewHolder(View view) {

            super(view);

            friendName=(TextView) view.findViewById(R.id.friend_name);
        }
    }

    public FriendsTagAdapter(Context mContext, List<Friend> itemList) {

        this.mContext = mContext;
        this.friendList = itemList;
        this.filterfriendList=itemList;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_tag_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Friend item = friendList.get(position);
        holder.friendName.setText("@"+item.getFriendUserUsername());

    }

    @Override
    public int getItemCount() {
        return friendList.size();
    }
    @Override
    public Filter getFilter() {
        if(userFilter == null)
            userFilter = new UserFilter(this, friendList);
        return userFilter;
    }

    private static class UserFilter extends Filter {

        private final FriendsTagAdapter adapter;

        private final List<Friend> originalList;

        private final List<Friend> filteredList;

        private UserFilter(FriendsTagAdapter adapter, List<Friend> originalList) {
            super();
            this.adapter = adapter;
            this.originalList = new LinkedList<>(originalList);
            this.filteredList = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();


            if (constraint.length() == 0) {
                filteredList.addAll(originalList);
            } else {
                final String filterPattern = constraint.toString().toLowerCase();

                for (final Friend user : originalList) {
                    Log.d("username=",user.getFriendUserFullname());
                   if (user.getFriendUserFullname().toLowerCase().contains(filterPattern)|| user.getFriendUserUsername().toLowerCase().contains(filterPattern)) {
                        filteredList.add(user);
                    }
                   /* if (user.getFriendUserUsername().toLowerCase().contains(filterPattern)) {
                        filteredList.add(user);
                    }*/
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            System.out.println("&& Size " + filteredList.size());
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.filterfriendList.clear();
            adapter.filterfriendList.addAll((ArrayList<Friend>) results.values);
            adapter.notifyDataSetChanged();
        }
    }
}
