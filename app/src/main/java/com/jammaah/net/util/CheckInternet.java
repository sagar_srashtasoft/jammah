package com.jammaah.net.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by pankajsakariya on 17/07/17.
 */

public class CheckInternet {

    public static boolean isInternetOn(Context context){
        boolean internetOn = false;

        final ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            internetOn =true;
        }else{
            internetOn =false;
        }

        return internetOn;
    }
}
