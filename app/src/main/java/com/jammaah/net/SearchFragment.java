package com.jammaah.net;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.jammaah.net.adapter.AutoCompleteDbAdapter;
import com.jammaah.net.adapter.SearchListAdapter;
import com.jammaah.net.app.App;
import com.jammaah.net.common.ActivityBase;
import com.jammaah.net.constants.Constants;
import com.jammaah.net.dialogs.SearchSettingsDialog;
import com.jammaah.net.model.Profile;
import com.jammaah.net.util.CustomRequest;
import com.jammaah.net.util.Helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class SearchFragment extends Fragment implements Constants, SwipeRefreshLayout.OnRefreshListener {



    SearchView searchView = null;

    RecyclerView mRecyclerView;
    TextView mMessage, mHeaderText, mHeaderSettings;

    ImageView mSplash,noInternate;

    LinearLayout mHeaderContainer,ll_name,ll_familyname,ll_location,ll_searchResult;

    SwipeRefreshLayout mItemsContainer;
    private AutoCompleteTextView actName, actFamily, actLocation;
    private ArrayList<Profile> itemsList;
    private ArrayList<Profile> itemsListsearch=new ArrayList<>();
    private SearchListAdapter itemsAdapter;
    private Button btn_search;
    public String queryText, currentQuery, oldQuery,oldFamilyname,oldlocation;

    private int search_gender = -1, search_online = -1, preload_gender = -1;
   private String familynamequery,locationquery;
    public int itemCount;
    private int userId = 0;
    private int itemId = 0;
    private int arrayLength = 0;
    private Boolean loadingMore = false;
    private Boolean viewMore = false;
    private Boolean restore = false;
    private Boolean preload = true;
    private Context mcontext;
    int pastVisiblesItems = 0, visibleItemCount = 0, totalItemCount = 0;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

       // AutoCompleteDbAdapter dbHelper = new AutoCompleteDbAdapter(getActivity());
        final View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        mcontext = getContext();

        if (savedInstanceState != null) {


            itemsAdapter = new SearchListAdapter(getActivity(), itemsList);

            currentQuery = queryText = savedInstanceState.getString("queryText");

            preload = savedInstanceState.getBoolean("preload");
            restore = savedInstanceState.getBoolean("restore");
            itemId = savedInstanceState.getInt("itemId");
            userId = savedInstanceState.getInt("userId");
            itemCount = savedInstanceState.getInt("itemCount");
            search_gender = savedInstanceState.getInt("search_gender");
            preload_gender = savedInstanceState.getInt("preload_gender");
            search_online = savedInstanceState.getInt("search_online");

        } else {

            itemsList = new ArrayList<Profile>();
            itemsAdapter = new SearchListAdapter(getActivity(), itemsList);

            currentQuery = queryText = "";

            preload = true;
            restore = false;
            itemId = 0;
            userId = 0;
            itemCount = 0;
            search_gender = -1;
            preload_gender = -1;
            search_online = -1;
        }
        ll_searchResult=(LinearLayout)rootView.findViewById(R.id.ll_searchResult);
        ll_name=(LinearLayout)rootView.findViewById(R.id.ll_name);
        ll_familyname=(LinearLayout)rootView.findViewById(R.id.ll_familyname);
        ll_location=(LinearLayout)rootView.findViewById(R.id.ll_location);
        btn_search=(Button)rootView.findViewById(R.id.btn_search);
        mHeaderContainer = (LinearLayout) rootView.findViewById(R.id.container_header);
        mHeaderText = (TextView) rootView.findViewById(R.id.headerText);
        mHeaderSettings = (TextView) rootView.findViewById(R.id.headerSettings);
        actName = (AutoCompleteTextView) rootView.findViewById(R.id.act_name);
        actFamily = (AutoCompleteTextView) rootView.findViewById(R.id.act_familyname);
        actLocation = (AutoCompleteTextView) rootView.findViewById(R.id.act_location);
        mItemsContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.container_items);
        mItemsContainer.setOnRefreshListener(this);

        mMessage = (TextView) rootView.findViewById(R.id.message);
        mSplash = (ImageView) rootView.findViewById(R.id.splash);
        noInternate = (ImageView) rootView.findViewById(R.id.noInternate);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        final LinearLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), Helper.getGridSpanCount(getActivity()));
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mRecyclerView.setAdapter(itemsAdapter);


        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                if (dy > 0) {

                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (!loadingMore) {

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount && (viewMore) && !(mItemsContainer.isRefreshing())) {

                            Log.e("...", "Last Item Wow !");

                            if (preload) {

                                loadingMore = true;

                                preload();

                            } else {

                                currentQuery = actName.getText().toString();

                                if (currentQuery.equals(oldQuery)||actFamily.getText().toString().equals(oldFamilyname)||actLocation.getText().toString().equals(oldlocation)) {

                                    loadingMore = true;

                                    search();
                                }
                            }
                        }
                    }
                }
            }
        });

        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {

                Profile u = (Profile) itemsList.get(position);

                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                intent.putExtra("profileId", u.getId());
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(View view, int position) {
                // ...
            }
        }));

        try{
        if (itemsAdapter.getItemCount() == 0) {

            showMessage(getText(R.string.label_empty_list).toString());

        } else {

            hideMessage();
        }}catch (Exception ex){
            ex.printStackTrace();
        }

        if (queryText.length() == 0) {

            if (itemsAdapter.getItemCount() == 0) {

                showMessage(getString(R.string.label_search_start_screen_msg));
                mHeaderText.setVisibility(View.GONE);

            } else {

                if (preload) {

                    mHeaderText.setVisibility(View.GONE);

                } else {

                    mHeaderText.setVisibility(View.VISIBLE);
                    mHeaderText.setText(getText(R.string.label_search_results) + " " + Integer.toString(itemCount));
                }

                hideMessage();
            }

        } else {

            if (itemsAdapter.getItemCount() == 0) {

                showMessage(getString(R.string.label_search_results_error));
                mHeaderText.setVisibility(View.GONE);

            } else {

                mHeaderText.setVisibility(View.VISIBLE);
                mHeaderText.setText(getText(R.string.label_search_results) + " " + Integer.toString(itemCount));

                hideMessage();
            }
        }

        mHeaderSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /** Getting the fragment manager */
                android.app.FragmentManager fm = getActivity().getFragmentManager();

                /** Instantiating the DialogFragment class */
                SearchSettingsDialog alert = new SearchSettingsDialog();

                /** Creating a bundle object to store the selected item's index */
                Bundle b = new Bundle();

                /** Storing the selected item's index in the bundle object */
                b.putInt("searchGender", search_gender);
                b.putInt("searchOnline", search_online);


                /** Setting the bundle object to the dialog fragment object */
                alert.setArguments(b);

                /** Creating the dialog fragment object, which will in turn open the alert dialog window */

                alert.show(fm, "alert_dialog_search_settings");

                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }
        });

        if (queryText.length() == 0) {

            if (itemsAdapter.getItemCount() == 0) {

                showMessage(getString(R.string.label_search_start_screen_msg));
                mHeaderContainer.setVisibility(View.VISIBLE);

            } else {

                mHeaderContainer.setVisibility(View.VISIBLE);
                mHeaderText.setText(getText(R.string.label_search_results) + " " + Integer.toString(itemCount));

                hideMessage();
            }

        } else {

            if (itemsAdapter.getItemCount() == 0) {

                showMessage(getString(R.string.label_search_results_error));
                mHeaderContainer.setVisibility(View.VISIBLE);

            } else {

                mHeaderContainer.setVisibility(View.VISIBLE);
                mHeaderText.setText(getText(R.string.label_search_results) + " " + Integer.toString(itemCount));

                hideMessage();
            }
        }

        if (!restore) {

            if (preload) {

                preload();
            }
        }
     //Add textListener when user search by name,Family and Location

            actName.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    searchByname();

                    if(itemsListsearch.size()!=0)
                    {
                        Random random=new Random();
                        int dbversion=random.nextInt(9999-1000)+1000;
                        AutoCompleteDbAdapter  dbHelper = new AutoCompleteDbAdapter(getActivity(),itemsListsearch,dbversion);
                        ItemAutoTextAdapter adapter = new ItemAutoTextAdapter(dbHelper);
                        actName.setAdapter(adapter);
                        actName.setOnItemClickListener(adapter);



                    }

                }
                @Override
                public void afterTextChanged(Editable editable) {
                }
            });

        actFamily.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                searchByname();

                if(itemsListsearch.size()!=0) {
                    Random random = new Random();
                    int dbversion = random.nextInt(9999 - 1000) + 1000;
                    AutoCompleteDbAdapter  dbHelper = new AutoCompleteDbAdapter(getActivity(),itemsListsearch,dbversion);
                    FamilyTextAdapter fAdapter = new FamilyTextAdapter(dbHelper);
                    actFamily.setAdapter(fAdapter);
                    actFamily.setOnItemClickListener(fAdapter);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

            actLocation.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    searchByname();

                    if(itemsListsearch.size()!=0) {
                        Random random = new Random();
                        int dbversion = random.nextInt(9999 - 1000) + 1000;
                        AutoCompleteDbAdapter  dbHelper = new AutoCompleteDbAdapter(getActivity(),itemsListsearch,dbversion);
                        LocationTextAdapter  lAdapter = new LocationTextAdapter(dbHelper);
                        actLocation.setAdapter(lAdapter);
                        actLocation.setOnItemClickListener(lAdapter);
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });


                btn_search.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   ActivityBase.hideKeyboard(getActivity());
                   preload=false;
                   userId=0;
                   currentQuery=actName.getText().toString().trim();
                search();


             }
        });

        //Click On Keyboard search Action
        actName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_SEARCH)
                {
                    preload=false;
                    ActivityBase.hideKeyboard(getActivity());
                    actionSearch();

                    return true;
                }
                return false;
            }

        });
        actFamily.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_SEARCH)
                {
                    ActivityBase.hideKeyboard(getActivity());
                    preload=false;
                    actionSearch();

                    return true;
                }
                return false;
            }

        });
        actLocation.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_SEARCH)
                {
                    ActivityBase.hideKeyboard(getActivity());
                    preload=false;
                    actionSearch();

                    return true;
                }
                return false;
            }

        });



        // Inflate the layout for this fragment
        return rootView;
    }
//imeSearch Action
    private void actionSearch() {
        userId=0;
        currentQuery=actName.getText().toString().trim();

        search();

    }

    //Name vise Search
    class ItemAutoTextAdapter extends CursorAdapter
            implements android.widget.AdapterView.OnItemClickListener {

        private AutoCompleteDbAdapter mDbHelper;
        private String searchText;
        public ItemAutoTextAdapter(AutoCompleteDbAdapter dbHelper) {
            // Call the CursorAdapter constructor with a null Cursor.
            super(mcontext, null);
            mDbHelper = dbHelper;
        }
        @Override
        public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
            if (getFilterQueryProvider() != null) {
                return getFilterQueryProvider().runQuery(constraint);
            }

            Cursor cursor = mDbHelper.getMatchingFullname(
                    (constraint != null ? constraint.toString() : null));

            return cursor;
        }

        @Override
        public String convertToString(Cursor cursor) {
            final int columnIndex = cursor.getColumnIndexOrThrow("fullname");
            final String str = cursor.getString(columnIndex);
            return str;
        }
        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            final String text = convertToString(cursor);
            ((TextView) view).setText(text);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            final LayoutInflater inflater = LayoutInflater.from(context);
            final View view =
                    inflater.inflate(R.layout.dropdown_search,
                            parent, false);
            return view;
        }


        @Override
        public void onItemClick(AdapterView<?> listView, View view, int position, long id) {
            // Get the cursor, positioned to the corresponding row in the result set
            Cursor cursor = (Cursor) listView.getItemAtPosition(position);

            // Get the state's capital from this row in the database.
            try {
                String location = cursor.getString(cursor.getColumnIndexOrThrow("location"));
            }catch(Exception ex) {
                ex.printStackTrace();
            }
            // Update the parent class's TextView
          //  mStateCapitalView.setText(capital);
        }

    }

    //Search By Location
    class LocationTextAdapter extends CursorAdapter
            implements android.widget.AdapterView.OnItemClickListener {

        private AutoCompleteDbAdapter mDbHelper;

        public LocationTextAdapter(AutoCompleteDbAdapter dbHelper) {
            // Call the CursorAdapter constructor with a null Cursor.
            super(mcontext, null);
            mDbHelper = dbHelper;
        }
        @Override
        public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
            if (getFilterQueryProvider() != null) {
                return getFilterQueryProvider().runQuery(constraint);
            }

            Cursor cursor = mDbHelper.getMatchingLocation(
                    (constraint != null ? constraint.toString() : null));

            return cursor;
        }

        @Override
        public String convertToString(Cursor cursor) {
            final int columnIndex = cursor.getColumnIndexOrThrow("location");
            final String str = cursor.getString(columnIndex);
            return str;
        }
        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            final String text = convertToString(cursor);
            ((TextView) view).setText(text);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            final LayoutInflater inflater = LayoutInflater.from(context);
            final View view =
                    inflater.inflate(R.layout.dropdown_search,
                            parent, false);
            return view;
        }


        @Override
        public void onItemClick(AdapterView<?> listView, View view, int position, long id) {
            // Get the cursor, positioned to the corresponding row in the result set
            Cursor cursor = (Cursor) listView.getItemAtPosition(position);

            // Get the state's capital from this row in the database.
            try {
                String location = cursor.getString(cursor.getColumnIndexOrThrow("location"));
            }catch(Exception ex) {
                ex.printStackTrace();
            }
            // Update the parent class's TextView
            //  mStateCapitalView.setText(capital);
        }

    }
//Search By FamilyName
    class FamilyTextAdapter extends CursorAdapter
            implements android.widget.AdapterView.OnItemClickListener {

        private AutoCompleteDbAdapter mDbHelper;

        public FamilyTextAdapter(AutoCompleteDbAdapter dbHelper) {
            // Call the CursorAdapter constructor with a null Cursor.
            super(mcontext, null);
            mDbHelper = dbHelper;
        }
        @Override
        public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
            if (getFilterQueryProvider() != null) {
                return getFilterQueryProvider().runQuery(constraint);
            }

            Cursor cursor = mDbHelper.getMatchingFname(
                    (constraint != null ? constraint.toString() : null));

            return cursor;
        }

        @Override
        public String convertToString(Cursor cursor) {
            final int columnIndex = cursor.getColumnIndexOrThrow("familyname");
            final String str = cursor.getString(columnIndex);
            return str;
        }
        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            final String text = convertToString(cursor);
            ((TextView) view).setText(text);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            final LayoutInflater inflater = LayoutInflater.from(context);
            final View view =
                    inflater.inflate(R.layout.dropdown_search,
                            parent, false);
            return view;
        }


        @Override
        public void onItemClick(AdapterView<?> listView, View view, int position, long id) {
            // Get the cursor, positioned to the corresponding row in the result set
            Cursor cursor = (Cursor) listView.getItemAtPosition(position);

            // Get the state's capital from this row in the database.
            try {
                String location = cursor.getString(cursor.getColumnIndexOrThrow("location"));
            }catch(Exception ex) {
                ex.printStackTrace();
            }
            // Update the parent class's TextView
            //  mStateCapitalView.setText(capital);
        }

    }


    private void searchByname() {

        CustomRequest jsonReq = new CustomRequest(Request.Method.POST, METHOD_APP_SEARCH, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        if (!isAdded() || getActivity() == null) {

                            Log.e("ERROR", "SearchFragment Not Added to Activity");

                            return;
                        }

                        try {


                            itemsListsearch.clear();
                            arrayLength = 0;

                            if (!response.getBoolean("error")) {

                                itemCount = response.getInt("itemCount");
                                oldQuery = response.getString("query");
                                userId = response.getInt("itemId");
                                oldFamilyname=response.getString("familyname");
                                oldlocation=response.getString("location");

                                if (response.has("items")) {

                                    JSONArray usersArray = response.getJSONArray("items");

                                    arrayLength = usersArray.length();

                                    if (arrayLength > 0) {

                                        Log.d("lenghtOFsearch=",usersArray.length()+"");
                                        for (int i = 0; i < usersArray.length(); i++) {

                                            JSONObject profileObj = (JSONObject) usersArray.get(i);

                                            Profile profile = new Profile(profileObj);

                                            itemsListsearch.add(profile);

                                        }
                                    }
                                }
                            }

                        } catch (JSONException e) {

                            e.printStackTrace();

                        }finally {

                            //itemsAdapter.notifyDataSetChanged();
                            if(itemsAdapter.getItemCount()==0){
                                showMessage(getString(R.string.label_search_results_error));
                            }else {
                                hideMessage();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (!isAdded() || getActivity() == null) {

                    Log.e("ERROR", "SearchFragment Not Added to Activity");

                    return;
                }


                Toast.makeText(getActivity(), getString(R.string.error_data_loading), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("accountId", Long.toString(App.getInstance().getId()));
                params.put("accessToken", App.getInstance().getAccessToken());
                params.put("query", actName.getText().toString());
                params.put("userId", Integer.toString(userId));
                params.put("gender", Integer.toString(search_gender));
                params.put("online", Integer.toString(search_online));

                return params;
            }
        };

        App.getInstance().addToRequestQueue(jsonReq);
    }



    public void onCloseSettingsDialog(int searchGender, int searchOnline) {

        search_gender = searchGender;
        search_online = searchOnline;

        String q = getCurrentQuery();

        if (preload) {

            itemId = 0;

            preload();

        } else {

            if (q.length() > 0) {

                searchStart();
            }
        }
    }

    @Override
    public void onRefresh() {

        currentQuery = queryText;

        currentQuery = currentQuery.trim();

        if (App.getInstance().isConnected() && currentQuery.length() != 0) {

            userId = 0;
            search();

        } else {

            mItemsContainer.setRefreshing(false);
        }
    }

    public String getCurrentQuery() {
        String searchText="";
        if(searchView != null) {
             searchText = searchView.getQuery().toString();
            searchText = searchText.trim();
        }

        return searchText;
    }

    public void searchStart() {

        preload = false;

        currentQuery = getCurrentQuery();

        if (App.getInstance().isConnected()) {

            userId = 0;
            search();

        } else {

            Toast.makeText(getActivity(), getText(R.string.msg_network_error), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);

        outState.putString("queryText", queryText);
        outState.putBoolean("restore", true);
        outState.putBoolean("preload", preload);
        outState.putInt("itemId", itemId);
        outState.putInt("userId", userId);
        outState.putInt("itemCount", itemCount);
        outState.putInt("search_gender", search_gender);
        outState.putInt("preload_gender", preload_gender);
        outState.putInt("search_online", search_online);

    }

    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

//        MenuInflater menuInflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        MenuItem searchItem = menu.findItem(R.id.options_menu_main_search);

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {

            searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        }

        if (searchView != null) {

            searchView.setQuery(queryText, false);

            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
            searchView.setIconifiedByDefault(false);
            searchView.setIconified(false);

            SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
            searchAutoComplete.setHint(getText(R.string.placeholder_search));
            searchAutoComplete.setHintTextColor(getResources().getColor(R.color.white));

            searchView.clearFocus();

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {

                    queryText = newText;

                    return false;
                }

                @Override
                public boolean onQueryTextSubmit(String query) {

                    queryText = query;
                    searchStart();

                    return false;
                }
            });
        }

        super.onCreateOptionsMenu(menu, inflater);
    }
*/
    public void search() {

        mItemsContainer.setRefreshing(true);

        if(actFamily.getText().toString().equalsIgnoreCase(""))
        {
             familynamequery="-1";

        }else {
            familynamequery=actFamily.getText().toString().trim();
        }
        if(actLocation.getText().toString().equalsIgnoreCase(""))
        {
            locationquery="-1";

        }else {
            locationquery=actLocation.getText().toString().trim();
        }
        if(currentQuery.equalsIgnoreCase("")||currentQuery.equalsIgnoreCase(null))
        {
            currentQuery="-1";

        }


        CustomRequest jsonReq = new CustomRequest(Request.Method.POST, METHOD_APP_SEARCH, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        if (!isAdded() || getActivity() == null) {

                            Log.e("ERROR", "SearchFragment Not Added to Activity");

                            return;
                        }

                        try {

                            if (!loadingMore) {

                                itemsList.clear();
                            }

                            arrayLength = 0;

                            if (!response.getBoolean("error")) {

                                itemCount = response.getInt("itemCount");
                                oldQuery = response.getString("query");
                                userId = response.getInt("itemId");
                                oldFamilyname=response.getString("familyname");
                                oldlocation=response.getString("location");
                                if (response.has("items")) {

                                    JSONArray usersArray = response.getJSONArray("items");

                                    arrayLength = usersArray.length();

                                    if (arrayLength > 0) {

                                        Log.d("lenghtOFsearch=",usersArray.length()+"");
                                        for (int i = 0; i < usersArray.length(); i++) {

                                            JSONObject profileObj = (JSONObject) usersArray.get(i);

                                            Profile profile = new Profile(profileObj);

                                            itemsList.add(profile);

                                        }
                                    }
                                }
                            }

                        } catch (JSONException e) {

                            e.printStackTrace();

                        } finally {

                            loadingComplete();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (!isAdded() || getActivity() == null) {

                    Log.e("ERROR", "SearchFragment Not Added to Activity");

                    return;
                }

                loadingComplete();
                Toast.makeText(getActivity(), getString(R.string.error_data_loading), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("accountId", Long.toString(App.getInstance().getId()));
                params.put("accessToken", App.getInstance().getAccessToken());
                params.put("query", currentQuery);
                params.put("familyname",familynamequery);
                params.put("location",locationquery);
                params.put("userId", Integer.toString(userId));
                params.put("gender", Integer.toString(search_gender));
                params.put("online", Integer.toString(search_online));

                return params;
            }
        };

        App.getInstance().addToRequestQueue(jsonReq);
    }

    public void preload() {

        if (preload) {

            mItemsContainer.setRefreshing(true);

            CustomRequest jsonReq = new CustomRequest(Request.Method.POST, METHOD_APP_SEARCH_PRELOAD, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            if (!isAdded() || getActivity() == null) {

                                Log.e("ERROR", "SearchFragment Not Added to Activity");

                                return;
                            }

                            try {

                                if (!loadingMore) {

                                    itemsList.clear();
                                }

                                arrayLength = 0;

                                if (!response.getBoolean("error")) {

                                    itemId = response.getInt("itemId");

                                    if (response.has("items")) {

                                     JSONArray usersArray = response.getJSONArray("items");

                                        arrayLength = usersArray.length();
                                        Log.d("ItemList=",arrayLength+"");
                                        if (arrayLength > 0) {

                                            for (int i = 0; i < usersArray.length(); i++) {

                                                JSONObject profileObj = (JSONObject) usersArray.get(i);

                                                Profile u = new Profile(profileObj);

                                                itemsList.add(u);

                                            }
                                        }
                                    }
                                }

                            } catch (JSONException e) {

                                e.printStackTrace();

                            } finally {

                                loadingComplete();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    if (!isAdded() || getActivity() == null) {

                        Log.e("ERROR", "SearchFragment Not Added to Activity");

                        return;
                    }

                    loadingComplete();
                    Toast.makeText(getActivity(), getString(R.string.error_data_loading), Toast.LENGTH_LONG).show();
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("accountId", Long.toString(App.getInstance().getId()));
                    params.put("accessToken", App.getInstance().getAccessToken());
                    params.put("itemId", Integer.toString(itemId));
                    params.put("gender", Integer.toString(search_gender));
                    params.put("online", Integer.toString(search_online));

                    return params;
                }
            };

            App.getInstance().addToRequestQueue(jsonReq);
        }
    }

    public void loadingComplete() {




        if (arrayLength == LIST_ITEMS) {

            viewMore = true;


        } else {

            viewMore = false;
        }

        itemsAdapter.notifyDataSetChanged();

        loadingMore = false;

        mItemsContainer.setRefreshing(false);

        if (itemsAdapter.getItemCount() == 0) {
            if(App.getInstance().isConnected()){
                noInternate.setVisibility(View.GONE);
                showMessage(getString(R.string.label_search_results_error));
                mHeaderText.setVisibility(View.GONE);
            }else{
                mMessage.setText("No Internet");
                ll_searchResult.setVisibility(View.VISIBLE);
                noInternate.setVisibility(View.VISIBLE);
               mSplash.setVisibility(View.GONE);

            }



        } else {

            hideMessage();

            if (preload) {

                mHeaderText.setVisibility(View.GONE);

            } else {

        /*        ll_name.setVisibility(View.GONE);
                ll_location.setVisibility(View.GONE);
                ll_familyname.setVisibility(View.GONE);*/
                mHeaderText.setVisibility(View.VISIBLE);

                mHeaderText.setText(getText(R.string.label_search_results) + " " + Integer.toString(itemCount));
            }
        }

    }

    public void showMessage(String message) {

        mMessage.setText(message);
       ll_searchResult.setVisibility(View.VISIBLE);
    }

    public void hideMessage() {

     ll_searchResult.setVisibility(View.GONE);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    static class RecyclerItemClickListener implements RecyclerView.OnItemTouchListener {

        public interface OnItemClickListener {

            void onItemClick(View view, int position);

            void onItemLongClick(View view, int position);
        }

        private OnItemClickListener mListener;

        private GestureDetector mGestureDetector;

        public RecyclerItemClickListener(Context context, final RecyclerView recyclerView, RecyclerItemClickListener.OnItemClickListener listener) {

            mListener = listener;

            mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {

                    View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());

                    if (childView != null && mListener != null) {

                        mListener.onItemLongClick(childView, recyclerView.getChildAdapterPosition(childView));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {

            View childView = view.findChildViewUnder(e.getX(), e.getY());

            if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {

                mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView view, MotionEvent motionEvent) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}