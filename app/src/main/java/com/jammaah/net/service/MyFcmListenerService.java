package com.jammaah.net.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.jammaah.net.AppActivity;
import com.jammaah.net.ChatFragment;
import com.jammaah.net.DialogsActivity;
import com.jammaah.net.FriendsActivity;
import com.jammaah.net.MainActivity;
import com.jammaah.net.NotificationsActivity;
import com.jammaah.net.R;
import com.jammaah.net.ViewImageActivity;
import com.jammaah.net.ViewItemActivity;
import com.jammaah.net.app.App;
import com.jammaah.net.constants.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class MyFcmListenerService extends FirebaseMessagingService implements Constants {
    private static int value = 0;
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    @Override
    public void onMessageReceived(RemoteMessage message) {
        String from = message.getFrom();
        Map data = message.getData();

        Log.e("Message", "Could not parse malformed JSON: \"" + data.toString() + "\"");

        generateNotification(getApplicationContext(), data);
    }

//    @Override
//    public void onMessageReceived(String from, Bundle data) {
//
//        generateNotification(getApplicationContext(), data);
//        Log.e("Message", "Could not parse malformed JSON: \"" + data.toString() + "\"");
//    }

    @Override
    public void onDeletedMessages() {
        sendNotification("Deleted messages on server");
    }

    @Override
    public void onMessageSent(String msgId) {

        sendNotification("Upstream message sent. Id=" + msgId);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String msg) {

        Log.e("Message", "Could not parse malformed JSON: \"" + msg + "\"");
    }

    /**
     * Create a notification to inform the user that server has sent a message.
     */
    private static void generateNotification(Context context, Map data) {

        String msgId = "0";
        String msgFromUserId = "0";
        String msgFromUserState = "0";
        String msgFromUserVerify = "0";
        String msgFromUserUsername = "";
        String msgFromUserFullname = "";
        String msgFromUserFamilyname="";
        String msgFromUserPhotoUrl = "";
        String msgMessage = "";
        String msgImgUrl = "";
        String msgCreateAt = "0";
        String msgDate = "";
        String msgTimeAgo = "";
        String msgRemoveAt = "0";
        sharedPreferences = context.getSharedPreferences("MysharedPref", MODE_PRIVATE);
        editor=sharedPreferences.edit();
        String message = data.get("msg").toString();
        String type = data.get("type").toString();
        String actionId = data.get("id").toString();
        String accountId = data.get("accountId").toString();

        if (Integer.valueOf(type) == GCM_NOTIFY_MESSAGE) {

            msgId = data.get("msgId").toString();
            msgFromUserId = data.get("msgFromUserId").toString();
            msgFromUserState = data.get("msgFromUserState").toString();
            msgFromUserVerify = data.get("msgFromUserVerify").toString();
            msgFromUserUsername = data.get("msgFromUserUsername").toString();
            msgFromUserFullname = data.get("msgFromUserFullname").toString();
            msgFromUserFamilyname=data.get("msgFromUserFamilyname").toString();
            msgFromUserPhotoUrl = data.get("msgFromUserPhotoUrl").toString();
            msgMessage = data.get("msgMessage").toString();
            msgImgUrl = data.get("msgImgUrl").toString();
            msgCreateAt = data.get("msgCreateAt").toString();
            msgDate = data.get("msgDate").toString();
            msgTimeAgo = data.get("msgTimeAgo").toString();
            msgRemoveAt = data.get("msgRemoveAt").toString();
        }

        int icon = R.drawable.ic_action_push_notification;
        long when = System.currentTimeMillis();

        String title = context.getString(R.string.app_name);

//        App.getInstance().reload();

        switch (Integer.valueOf(type)) {

            case GCM_NOTIFY_SYSTEM: {

                RemoteViews remoteview=new RemoteViews(context.getPackageName(),R.layout.custome_notification);
                remoteview.setImageViewResource(R.id.iv_notificationIcon,R.drawable.notification_icon);
                remoteview.setTextViewText(R.id.tv_notificationTitle,title);
                Spannable messageColor=new SpannableString(message);
                Linkify.addLinks(messageColor,Linkify.WEB_URLS);
                remoteview.setTextViewText(R.id.tv_message,messageColor);
                Log.d("systemNotifi=","systemNotifi");
                Random random=new Random();
                int notificationId=random.nextInt(9999-1000)+1000;
                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(context)
                                .setSmallIcon(getNotificationIcon())
                                .setContentTitle(title)
                                .setContent(remoteview);

                Intent resultIntent;

                if (App.getInstance().getId() != 0) {

                        if(message.contains("https://jammaah.net")) {
                            StringBuffer messageReverse = new StringBuffer(message);
                            messageReverse.reverse();
                            int messageindex = messageReverse.indexOf("/");
                            message = messageReverse.substring(0, messageindex);
                            StringBuffer postID = new StringBuffer(message);
                            postID.reverse();
                            resultIntent = new Intent(context, ViewItemActivity.class);
                            resultIntent.putExtra("itemId", Long.valueOf(postID.toString()));
                            resultIntent.putExtra("notificationFlag",1);
                            resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                            stackBuilder.addParentStack(ViewItemActivity.class);
                            stackBuilder.addNextIntent(resultIntent);
                            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                            mBuilder.setContentIntent(resultPendingIntent);
                        }else if(message.contains("http"))
                        {
                            StringBuffer messageReverse = new StringBuffer(message);
                            messageReverse.reverse();
                            int messageindex = messageReverse.indexOf(":");
                            message = messageReverse.substring(0, messageindex);
                            StringBuffer url = new StringBuffer(message);
                            url.reverse();
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                            Log.d("url",message);
                            browserIntent.setData(Uri.parse("http:"+url));
                            PendingIntent pending = PendingIntent.getActivity(context,0, browserIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                            mBuilder.setContentIntent(pending);
                        }else{
                            resultIntent = new Intent(context, MainActivity.class);
                            resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                            stackBuilder.addParentStack(MainActivity.class);
                            stackBuilder.addNextIntent(resultIntent);
                            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                            mBuilder.setContentIntent(resultPendingIntent);
                        }

                  //  resultIntent.putExtra("message",message);

                } else {

                    resultIntent = new Intent(context, AppActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                    stackBuilder.addParentStack(AppActivity.class);
                    stackBuilder.addNextIntent(resultIntent);
                    PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    mBuilder.setContentIntent(resultPendingIntent);
                }


                NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
                mBuilder.setAutoCancel(true);
                mNotificationManager.notify(0, mBuilder.build());

                break;
            }

            case GCM_NOTIFY_CUSTOM: {

                if (App.getInstance().getId() != 0) {
                    RemoteViews remoteview=new RemoteViews(context.getPackageName(),R.layout.custome_notification);
                    remoteview.setImageViewResource(R.id.iv_notificationIcon,R.drawable.notification_icon);
                    remoteview.setTextViewText(R.id.tv_notificationTitle,title);
                    Spannable messageColor=new SpannableString(message);
                    Linkify.addLinks(messageColor,Linkify.WEB_URLS);
                    remoteview.setTextViewText(R.id.tv_message,messageColor);
                    Log.d("systemNotifi=","systemNotifi2");
                    Random random=new Random();
                    int notificationId=random.nextInt(9999-1000)+1000;
                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(getNotificationIcon())
                                    .setContentTitle(title)
                                    .setContent(remoteview);

                    Intent resultIntent = new Intent(context, MainActivity.class);
                    resultIntent.putExtra("message",message);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                    stackBuilder.addParentStack(MainActivity.class);
                    stackBuilder.addNextIntent(resultIntent);
                    PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    mBuilder.setContentIntent(resultPendingIntent);
                    NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
                    mBuilder.setAutoCancel(true);
                    mNotificationManager.notify(0, mBuilder.build());
                }

                break;
            }

            case GCM_NOTIFY_PERSONAL: {

                if (App.getInstance().getId() != 0 && Long.toString(App.getInstance().getId()).equals(accountId)) {
                    RemoteViews remoteview=new RemoteViews(context.getPackageName(),R.layout.custome_notification);
                    remoteview.setImageViewResource(R.id.iv_notificationIcon,R.drawable.notification_icon);
                    remoteview.setTextViewText(R.id.tv_notificationTitle,title);
                    remoteview.setTextViewText(R.id.tv_message,message);
                    Log.d("systemNotifi=","systemNotifi3");
                    Random random=new Random();
                    int notificationId=random.nextInt(9999-1000)+1000;
                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(getNotificationIcon())
                                    .setContentTitle(title)
                                    .setContent(remoteview);

                    Intent resultIntent = new Intent(context, MainActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                    stackBuilder.addParentStack(MainActivity.class);
                    stackBuilder.addNextIntent(resultIntent);
                    PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    mBuilder.setContentIntent(resultPendingIntent);
                    NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
                    mBuilder.setAutoCancel(true);
                    mNotificationManager.notify(0, mBuilder.build());
                }

                break;
            }

            case GCM_NOTIFY_LIKE: {

                if (App.getInstance().getId() != 0 && Long.toString(App.getInstance().getId()).equals(accountId)) {

                    App.getInstance().setNotificationsCount(App.getInstance().getNotificationsCount() + 1);
                    int notificationCount=App.getInstance().getNotificationsCount();
                  //  message = context.getString(R.string.label_gcm_like);
                    String cmtfullnamename=data.get("fromUserFullname").toString();
                    String cmtFamilyname=data.get("fromUserFamilyname").toString();
                    /*NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.ic_action_push_notification)
                                    .setContentTitle(title)
                                    .setContentText(message);*/
                    //Notification unique Id's
                    Random random=new Random();
                    int notificationId=random.nextInt(9999-1000)+1000;
                    //Custom Notification
                    RemoteViews remoteview=new RemoteViews(context.getPackageName(),R.layout.custome_notification);

                  /*  Spannable messageColor=new SpannableString("Username"+"familyName"+message);
                    messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#d9be7f")),0,"Username".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#729ecf")),"Username".length(),"Username".length()+"familyName".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageColor.setSpan(new ForegroundColorSpan(Color.parseColor("#FF6B6767")),"Username".length()+"familyName".length(),messageColor.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);*/
                    remoteview.setImageViewResource(R.id.iv_notificationIcon,R.drawable.notification_icon);
                    remoteview.setTextViewText(R.id.tv_notificationTitle,title);
                    remoteview.setTextViewText(R.id.tv_message, cmtfullnamename+" "+cmtFamilyname+" "+message);

                    NotificationCompat.Builder mBuilder;

                    if(!( notificationCount>1)) {
                        mBuilder =
                                new NotificationCompat.Builder(context)
                                        .setSmallIcon(getNotificationIcon())
                                        .setContentTitle(title)
                                        .setContent(remoteview);

                    }else {
                        message="You've got "+notificationCount+" new likes";
                        remoteview.setTextViewText(R.id.tv_message,message);
                        mBuilder= new NotificationCompat.Builder(context)
                                .setSmallIcon(getNotificationIcon())
                                .setContentTitle(title)
                                .setContent(remoteview);
                    }


                    Intent resultIntent = new Intent(context, NotificationsActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                    stackBuilder.addParentStack(NotificationsActivity.class);
                    stackBuilder.addNextIntent(resultIntent);
                    PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    mBuilder.setContentIntent(resultPendingIntent);
                    NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
                    mBuilder.setAutoCancel(true);
                    mNotificationManager.notify(0, mBuilder.build());
                }

                break;
            }

            case GCM_NOTIFY_IMAGE_LIKE: {

                if (App.getInstance().getId() != 0 && Long.toString(App.getInstance().getId()).equals(accountId)) {

                    App.getInstance().setNotificationsCount(App.getInstance().getNotificationsCount() + 1);
                    int notificationCount=App.getInstance().getNotificationsCount();
                   /* NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.ic_action_push_notification)
                                    .setContentTitle(title)
                                    .setContentText(message);*/
                    //Notification unique Id's
                    Random random=new Random();
                    int notificationId=random.nextInt(9999-1000)+1000;
                    //Custom Notification
                    RemoteViews remoteview=new RemoteViews(context.getPackageName(),R.layout.custome_notification);
                   /* Spannable messageColor=new SpannableString("Username"+"familyName"+message);
                    messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#d9be7f")),0,"Username".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#729ecf")),"Username".length(),"Username".length()+"familyName".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageColor.setSpan(new ForegroundColorSpan(Color.parseColor("#FF6B6767")),"Username".length()+"familyName".length(),messageColor.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                   */ remoteview.setImageViewResource(R.id.iv_notificationIcon,R.drawable.notification_icon);
                    remoteview.setTextViewText(R.id.tv_notificationTitle,title);
                    remoteview.setTextViewText(R.id.tv_message, message);

                    NotificationCompat.Builder mBuilder;

                    if(!( notificationCount>1)) {
                        mBuilder =
                                new NotificationCompat.Builder(context)
                                        .setSmallIcon(getNotificationIcon())
                                        .setContentTitle(title)
                                        .setContent(remoteview);

                    }else {
                        message="You've got "+notificationCount+" new image likes";
                        mBuilder= new NotificationCompat.Builder(context)
                                .setSmallIcon(getNotificationIcon())
                                .setContentTitle(title)
                                .setContentText(message);
                    }

                    Intent resultIntent = new Intent(context, NotificationsActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                    stackBuilder.addParentStack(NotificationsActivity.class);
                    stackBuilder.addNextIntent(resultIntent);
                    PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    mBuilder.setContentIntent(resultPendingIntent);
                    NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
                    mBuilder.setAutoCancel(true);
                    mNotificationManager.notify(0, mBuilder.build());
                }

                break;
            }

            case GCM_NOTIFY_VIDEO_LIKE: {

                if (App.getInstance().getId() != 0 && Long.toString(App.getInstance().getId()).equals(accountId)) {

                    App.getInstance().setNotificationsCount(App.getInstance().getNotificationsCount() + 1);
                    int notificationCount=App.getInstance().getNotificationsCount();
                    message = context.getString(R.string.label_gcm_like);

             /*       NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.ic_action_push_notification)
                                    .setContentTitle(title)
                                    .setContentText(message);
*/
                    Random random=new Random();
                    int notificationId=random.nextInt(9999-1000)+1000;
                    //Custom Notification
                    RemoteViews remoteview=new RemoteViews(context.getPackageName(),R.layout.custome_notification);
                   /* Spannable messageColor=new SpannableString("Username"+"familyName"+message);
                    messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#d9be7f")),0,"Username".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#729ecf")),"Username".length(),"Username".length()+"familyName".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageColor.setSpan(new ForegroundColorSpan(Color.parseColor("#FF6B6767")),"Username".length()+"familyName".length(),messageColor.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                   */ remoteview.setImageViewResource(R.id.iv_notificationIcon,R.drawable.notification_icon);
                    remoteview.setTextViewText(R.id.tv_notificationTitle,title);
                    remoteview.setTextViewText(R.id.tv_message, message);

                    NotificationCompat.Builder mBuilder;

                    if(!( notificationCount>1)) {
                        mBuilder =
                                new NotificationCompat.Builder(context)
                                        .setSmallIcon(getNotificationIcon())
                                        .setContentTitle(title)
                                        .setContent(remoteview);

                    }else {
                        message="You've got "+notificationCount+" new video likes";
                        remoteview.setTextViewText(R.id.tv_message, message);
                        mBuilder= new NotificationCompat.Builder(context)
                                .setSmallIcon(getNotificationIcon())
                                .setContentTitle(title)
                                .setContent(remoteview);
                    }

                    Intent resultIntent = new Intent(context, NotificationsActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                    stackBuilder.addParentStack(NotificationsActivity.class);
                    stackBuilder.addNextIntent(resultIntent);
                    PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    mBuilder.setContentIntent(resultPendingIntent);
                    NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
                    mBuilder.setAutoCancel(true);
                    mNotificationManager.notify(0, mBuilder.build());
                }

                break;
            }

            case GCM_NOTIFY_FOLLOWER: {

                if (App.getInstance().getId() != 0 && Long.toString(App.getInstance().getId()).equals(accountId)) {

                    App.getInstance().setNotificationsCount(App.getInstance().getNotificationsCount() + 1);
                    int notificationCount=App.getInstance().getNotificationsCount();
                   // message = context.getString(R.string.label_gcm_friend_request);

                    String cmtfullnamename=data.get("fromUserFullname").toString();
                    String cmtFamilyname=data.get("fromUserFamilyname").toString();
/*
                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.ic_action_push_notification)
                                    .setContentTitle(title)
                                    .setContentText(message);*/

                    //Notification unique ID's
                    Random random=new Random();
                    int notificationId=random.nextInt(9999-1000)+1000;
                    //Custom Notification
                    RemoteViews remoteview=new RemoteViews(context.getPackageName(),R.layout.custome_notification);
                  /*  Spannable messageColor=new SpannableString("Username"+"familyName"+message);
                    messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#d9be7f")),0,"Username".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#729ecf")),"Username".length(),"Username".length()+"familyName".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageColor.setSpan(new ForegroundColorSpan(Color.parseColor("#FF6B6767")),"Username".length()+"familyName".length(),messageColor.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                  */  remoteview.setImageViewResource(R.id.iv_notificationIcon,R.drawable.notification_icon);
                    remoteview.setTextViewText(R.id.tv_notificationTitle,title);
                    remoteview.setTextViewText(R.id.tv_message, cmtfullnamename+" "+cmtFamilyname+" "+message);

                    NotificationCompat.Builder mBuilder= new NotificationCompat.Builder(context)
                                        .setSmallIcon(getNotificationIcon())
                                        .setContentTitle(title)
                                        .setColor(Color.parseColor("#7C3D8C"))
                                        .setContent(remoteview);

                    Intent resultIntent = new Intent(context, NotificationsActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                    stackBuilder.addParentStack(NotificationsActivity.class);
                    stackBuilder.addNextIntent(resultIntent);
                    PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    mBuilder.setContentIntent(resultPendingIntent);
                    NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
                    mBuilder.setAutoCancel(true);
                    mNotificationManager.notify(notificationId, mBuilder.build());
                }

                break;
            }

            case GCM_FRIEND_REQUEST_ACCEPTED: {

                if (App.getInstance().getId() != 0 && Long.toString(App.getInstance().getId()).equals(accountId)) {

                    App.getInstance().setNewFriendsCount(App.getInstance().getNewFriendsCount() + 1);
                    int notificationCount=App.getInstance().getNotificationsCount();
                   // message = context.getString(R.string.label_gcm_friend_request_accepted);
                    String cmtfullnamename=data.get("fromUserFullname").toString();
                    String cmtFamilyname=data.get("fromUserFamilyname").toString();
/*
                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.ic_action_push_notification)
                                    .setContentTitle(title)
                                    .setContentText(message);*/
                    //Notification unique Id's
                    Random random=new Random();
                    int notificationId=random.nextInt(9999-1000)+1000;
                    //Custom Notification
                    RemoteViews remoteview=new RemoteViews(context.getPackageName(),R.layout.custome_notification);

                 /*   Spannable messageColor=new SpannableString("Username"+"familyName"+message);
                    messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#d9be7f")),0,"Username".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#729ecf")),"Username".length(),"Username".length()+"familyName".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageColor.setSpan(new ForegroundColorSpan(Color.parseColor("#FF6B6767")),"Username".length()+"familyName".length(),messageColor.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                 */   remoteview.setImageViewResource(R.id.iv_notificationIcon,R.drawable.notification_icon);
                    remoteview.setTextViewText(R.id.tv_notificationTitle,title);
                    remoteview.setTextViewText(R.id.tv_message, cmtfullnamename+" "+cmtFamilyname+" "+message);

                    NotificationCompat.Builder mBuilder=
                                new NotificationCompat.Builder(context)
                                        .setSmallIcon(getNotificationIcon())
                                        .setColor(Color.parseColor("#7C3D8C"))
                                        .setContentTitle(title)
                                        .setContent(remoteview);



                    Intent resultIntent = new Intent(context, FriendsActivity.class);
                    resultIntent.putExtra("gcm", true);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                    stackBuilder.addParentStack(FriendsActivity.class);
                    stackBuilder.addNextIntent(resultIntent);
                    PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    mBuilder.setContentIntent(resultPendingIntent);
                    NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
                    mBuilder.setAutoCancel(true);
                    mNotificationManager.notify(notificationId, mBuilder.build());
                }

                break;
            }

            case GCM_NOTIFY_COMMENT: {

                if (App.getInstance().getId() != 0 && Long.toString(App.getInstance().getId()).equals(accountId)) {

                    App.getInstance().setNotificationsCount(App.getInstance().getNotificationsCount() + 1);
                    int notificationCount=App.getInstance().getNotificationsCount();
                   // Log.d("NotificationCount=",App.getInstance().getNotificationsCount() + 1+"");
                   // message = context.getString(R.string.label_gcm_comment);
                    //Notification Uniques Id's
                    HashMap<Integer,Integer> separatNotification=new HashMap<Integer,Integer>();
                    separatNotification=App.getInstance().getCommentList();




                    int postId=0;
                    int count=0;
                    for(Map.Entry notificationId:separatNotification.entrySet())
                    {

                        if(Integer.valueOf(actionId)==(int)notificationId.getKey())
                        {

                            count=(int) notificationId.getValue();

                            //separatNotification.remove(notificationId.getKey());

                        }
                    }
                    separatNotification.put(Integer.valueOf(actionId),count+1);
                    App.getInstance().setCommentList(separatNotification);

                    //Custom Notification
                    RemoteViews remoteview=new RemoteViews(context.getPackageName(),R.layout.custome_notification);
                    String cmtfullnamename=data.get("fromUserFullname").toString();
                    String cmtFamilyname=data.get("fromUserFamilyname").toString();
                    Log.d("CommentDetails=",accountId+cmtFamilyname);
                   /* Spannable messageColor=new SpannableString(cmtUsername+" "+cmtFamilyname+" "+message);
                    messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#FFFFFF")),0,cmtUsername.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#FFFFFF")),cmtUsername.length(),cmtUsername.length()+cmtFamilyname.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageColor.setSpan(new ForegroundColorSpan(Color.parseColor("#FFFFFF")),cmtUsername.length()+cmtFamilyname.length(),messageColor.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);*/
                    remoteview.setImageViewResource(R.id.iv_notificationIcon,R.drawable.notification_icon);
                    remoteview.setTextViewText(R.id.tv_notificationTitle,title);


                    NotificationCompat.Builder mBuilder;

                    if(count==0) {
                        remoteview.setTextViewText(R.id.tv_message, cmtfullnamename+" "+cmtFamilyname+" "+message);
                       mBuilder =
                        new NotificationCompat.Builder(context)
                                .setSmallIcon(getNotificationIcon())
                                .setColor(Color.parseColor("#7C3D8C"))
                                .setContentTitle(title)

                                .setContent(remoteview);

                    }else {
                        message="You've got "+(count+1)+" new comments";
                        remoteview.setTextViewText(R.id.tv_message, message);
                        mBuilder= new NotificationCompat.Builder(context)
                        .setSmallIcon(getNotificationIcon())
                        .setColor(Color.parseColor("#6b0b8c"))
                        .setContentTitle(title)

                        .setContent(remoteview);
                            }

                    Intent resultIntent = new Intent(context, NotificationsActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                    stackBuilder.addParentStack(NotificationsActivity.class);
                    stackBuilder.addNextIntent(resultIntent);
                    PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    mBuilder.setContentIntent(resultPendingIntent);
                    NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
                    mBuilder.setAutoCancel(true);
                    mNotificationManager.notify(Integer.parseInt(actionId), mBuilder.build());
                }

                break;
            }

            case GCM_NOTIFY_IMAGE_COMMENT: {

                if (App.getInstance().getId() != 0 && Long.toString(App.getInstance().getId()).equals(accountId)) {

                    App.getInstance().setNotificationsCount(App.getInstance().getNotificationsCount() + 1);
                    int notificationCount=App.getInstance().getNotificationsCount();
                   // message = context.getString(R.string.label_gcm_comment);
                    //Notification Uniques Id's
                    Random random=new Random();
                    int notificationId=random.nextInt(9999-1000)+1000;
                    //Custom Notification
                    RemoteViews remoteview=new RemoteViews(context.getPackageName(),R.layout.custome_notification);

                   /* Spannable messageColor=new SpannableString("Username"+"familyName"+message);
                    messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#d9be7f")),0,"Username".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#729ecf")),"Username".length(),"Username".length()+"familyName".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageColor.setSpan(new ForegroundColorSpan(Color.parseColor("#FF6B6767")),"Username".length()+"familyName".length(),messageColor.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                   */ remoteview.setImageViewResource(R.id.iv_notificationIcon,getNotificationIcon());
                    remoteview.setTextViewText(R.id.tv_notificationTitle,title);


                    NotificationCompat.Builder mBuilder;

                    if(!( notificationCount>1)) {
                        remoteview.setTextViewText(R.id.tv_message, message);
                        mBuilder =
                                new NotificationCompat.Builder(context)
                                        .setSmallIcon(getNotificationIcon())
                                        .setColor(Color.parseColor("#7C3D8C"))
                                        .setContentTitle(title)
                                        .setContent(remoteview);

                    }else {
                        message="You've got "+notificationCount+" new image comments";
                        remoteview.setTextViewText(R.id.tv_message, message);
                        mBuilder= new NotificationCompat.Builder(context)
                                .setSmallIcon(getNotificationIcon())
                                .setColor(Color.parseColor("#7C3D8C"))
                                .setContentTitle(title)
                                .setContent(remoteview);
                    }

        /*            NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.ic_action_push_notification)
                                    .setContentTitle(title)
                                    .setContentText(message);*/


                    Intent resultIntent = new Intent(context, NotificationsActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                    stackBuilder.addParentStack(NotificationsActivity.class);
                    stackBuilder.addNextIntent(resultIntent);

                    PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    mBuilder.setContentIntent(resultPendingIntent);
                    NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
                    mBuilder.setAutoCancel(true);
                    mNotificationManager.notify(0, mBuilder.build());
                }

                break;
            }

            case GCM_NOTIFY_VIDEO_COMMENT: {

                if (App.getInstance().getId() != 0 && Long.toString(App.getInstance().getId()).equals(accountId)) {

                    App.getInstance().setNotificationsCount(App.getInstance().getNotificationsCount() + 1);
                    int notificationCount=App.getInstance().getNotificationsCount();
                 //   message = context.getString(R.string.label_gcm_comment);

                   /* NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.ic_action_push_notification)
                                    .setContentTitle(title)
                                    .setContentText(message);*/
                    //Notification Uniques Id's
                    Random random=new Random();
                    int notificationId=random.nextInt(9999-1000)+1000;
                    //Custom Notification
                    RemoteViews remoteview=new RemoteViews(context.getPackageName(),R.layout.custome_notification);

                   /* Spannable messageColor=new SpannableString("Username"+"familyName"+message);
                    messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#d9be7f")),0,"Username".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#729ecf")),"Username".length(),"Username".length()+"familyName".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageColor.setSpan(new ForegroundColorSpan(Color.parseColor("#FF6B6767")),"Username".length()+"familyName".length(),messageColor.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                   */ remoteview.setImageViewResource(R.id.iv_notificationIcon,R.drawable.notification_icon);
                    remoteview.setTextViewText(R.id.tv_notificationTitle,title);
                    remoteview.setTextViewText(R.id.tv_message, message);

                    NotificationCompat.Builder mBuilder;

                    if(!( notificationCount>1)) {
                        mBuilder =
                                new NotificationCompat.Builder(context)
                                        .setSmallIcon(getNotificationIcon())
                                        .setContentTitle(title)
                                        .setContent(remoteview);

                    }else {
                        message="You've got "+notificationCount+" new video comments";
                        mBuilder= new NotificationCompat.Builder(context)
                                .setSmallIcon(getNotificationIcon())
                                .setContentTitle(title)
                                .setContentText(message);
                    }


                    Intent resultIntent = new Intent(context, NotificationsActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                    stackBuilder.addParentStack(NotificationsActivity.class);
                    stackBuilder.addNextIntent(resultIntent);
                    PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    mBuilder.setContentIntent(resultPendingIntent);
                    NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
                    mBuilder.setAutoCancel(true);
                    mNotificationManager.notify(0, mBuilder.build());
                }

                break;
            }

            case GCM_NOTIFY_COMMENT_REPLY: {

                if (App.getInstance().getId() != 0 && Long.toString(App.getInstance().getId()).equals(accountId)) {

                    App.getInstance().setNotificationsCount(App.getInstance().getReplyCount() + 1);
                    int notificationCount=App.getInstance().getReplyCount();
                //    message = context.getString(R.string.label_gcm_comment_reply);
/*
                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.ic_action_push_notification)
                                    .setContentTitle(title)
                                    .setContentText(message);*/
                    //Notification Uniques Id's
                    Random random=new Random();
                    int notificationId=random.nextInt(9999-1000)+1000;
                    String cmtfullnamename=data.get("fromUserFullname").toString();
                    String cmtFamilyname=data.get("fromUserFamilyname").toString();
                    //Custom Notification
                    RemoteViews remoteview=new RemoteViews(context.getPackageName(),R.layout.custome_notification);

                   /* Spannable messageColor=new SpannableString("Username"+"familyName"+message);
                    messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#d9be7f")),0,"Username".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#729ecf")),"Username".length(),"Username".length()+"familyName".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageColor.setSpan(new ForegroundColorSpan(Color.parseColor("#FF6B6767")),"Username".length()+"familyName".length(),messageColor.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                   */ remoteview.setImageViewResource(R.id.iv_notificationIcon,R.drawable.notification_icon);
                    remoteview.setTextViewText(R.id.tv_notificationTitle,title);


                    NotificationCompat.Builder mBuilder;

                    if(!( notificationCount>1)) {
                        remoteview.setTextViewText(R.id.tv_message, cmtfullnamename+" "+cmtFamilyname+" "+message);
                        mBuilder =
                                new NotificationCompat.Builder(context)
                                        .setSmallIcon(getNotificationIcon())
                                        .setContentTitle(title)
                                        .setContent(remoteview);

                    }else {
                        message="You've got "+notificationCount+" new comment reply";
                        remoteview.setTextViewText(R.id.tv_message, message);
                        mBuilder= new NotificationCompat.Builder(context)
                                .setSmallIcon(getNotificationIcon())
                                .setContentTitle(title)
                                .setContent(remoteview);
                    }


                    Intent resultIntent = new Intent(context, NotificationsActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                    stackBuilder.addParentStack(NotificationsActivity.class);
                    stackBuilder.addNextIntent(resultIntent);
                    PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    mBuilder.setContentIntent(resultPendingIntent);
                    NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
                    mBuilder.setAutoCancel(true);
                    mNotificationManager.notify(1, mBuilder.build());
                }

                break;
            }

            case GCM_NOTIFY_IMAGE_COMMENT_REPLY: {

                if (App.getInstance().getId() != 0 && Long.toString(App.getInstance().getId()).equals(accountId)) {

                    App.getInstance().setNotificationsCount(App.getInstance().getNotificationsCount() + 1);
                    int notificationCount=App.getInstance().getNotificationsCount();
                    message = context.getString(R.string.label_gcm_comment_reply);

                   /* NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.ic_action_push_notification)
                                    .setContentTitle(title)
                                    .setContentText(message);*/
                    //Notification Uniques Id's
                    Random random=new Random();
                    int notificationId=random.nextInt(9999-1000)+1000;
                    //Custom Notification
                    RemoteViews remoteview=new RemoteViews(context.getPackageName(),R.layout.custome_notification);

                   /* Spannable messageColor=new SpannableString("Username"+"familyName"+message);
                    messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#d9be7f")),0,"Username".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#729ecf")),"Username".length(),"Username".length()+"familyName".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageColor.setSpan(new ForegroundColorSpan(Color.parseColor("#FF6B6767")),"Username".length()+"familyName".length(),messageColor.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                   */ remoteview.setImageViewResource(R.id.iv_notificationIcon,R.drawable.notification_icon);
                    remoteview.setTextViewText(R.id.tv_notificationTitle,title);
                    remoteview.setTextViewText(R.id.tv_message, message);

                    NotificationCompat.Builder mBuilder;

                    if(!( notificationCount>1)) {
                        mBuilder =
                                new NotificationCompat.Builder(context)
                                        .setSmallIcon(getNotificationIcon())
                                        .setContentTitle(title)
                                        .setContent(remoteview);

                    }else {
                        message="You've got "+notificationCount+" new image comment reply";
                        mBuilder= new NotificationCompat.Builder(context)
                                .setSmallIcon(getNotificationIcon())
                                .setContentTitle(title)
                                .setContentText(message);
                    }


                    Intent resultIntent = new Intent(context, NotificationsActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                    stackBuilder.addParentStack(NotificationsActivity.class);
                    stackBuilder.addNextIntent(resultIntent);
                    PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    mBuilder.setContentIntent(resultPendingIntent);
                    NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
                    mBuilder.setAutoCancel(true);
                    mNotificationManager.notify(0, mBuilder.build());
                }

                break;
            }

            case GCM_NOTIFY_VIDEO_COMMENT_REPLY: {

                if (App.getInstance().getId() != 0 && Long.toString(App.getInstance().getId()).equals(accountId)) {

                    App.getInstance().setNotificationsCount(App.getInstance().getNotificationsCount() + 1);
                    int notificationCount=App.getInstance().getNotificationsCount();
                    message = context.getString(R.string.label_gcm_comment_reply);

                  /*  NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.ic_action_push_notification)
                                    .setContentTitle(title)
                                    .setContentText(message);*/
                    //Notification Uniques Id's
                    Random random=new Random();
                    int notificationId=random.nextInt(9999-1000)+1000;
                    //Custom Notification
                    RemoteViews remoteview=new RemoteViews(context.getPackageName(),R.layout.custome_notification);

                    Spannable messageColor=new SpannableString("Username"+"familyName"+message);
                    messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#d9be7f")),0,"Username".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#729ecf")),"Username".length(),"Username".length()+"familyName".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageColor.setSpan(new ForegroundColorSpan(Color.parseColor("#FF6B6767")),"Username".length()+"familyName".length(),messageColor.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    remoteview.setImageViewResource(R.id.iv_notificationIcon,R.drawable.ic_action_push_notification);
                    remoteview.setTextViewText(R.id.tv_notificationTitle,title);
                    remoteview.setTextViewText(R.id.tv_message, messageColor);

                    NotificationCompat.Builder mBuilder;

                    if(!( notificationCount>1)) {
                        mBuilder =
                                new NotificationCompat.Builder(context)
                                        .setSmallIcon(R.drawable.rsz_splash_icon)
                                        .setContentTitle(title)
                                        .setContent(remoteview);

                    }else {
                        message="You've got "+notificationCount+" new video comment reply";
                        mBuilder= new NotificationCompat.Builder(context)
                                .setSmallIcon(R.drawable.rsz_splash_icon)
                                .setContentTitle(title)

                                .setContentText(message);
                    }


                    Intent resultIntent = new Intent(context, NotificationsActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                    stackBuilder.addParentStack(NotificationsActivity.class);
                    stackBuilder.addNextIntent(resultIntent);
                    PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    mBuilder.setContentIntent(resultPendingIntent);
                    NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
                    mBuilder.setAutoCancel(true);
                    mNotificationManager.notify(0, mBuilder.build());
                }

                break;
            }

            case GCM_NOTIFY_GIFT: {

                if (App.getInstance().getId() != 0 && Long.toString(App.getInstance().getId()).equals(accountId)) {

                    App.getInstance().setNotificationsCount(App.getInstance().getNotificationsCount() + 1);

                    message = context.getString(R.string.label_gcm_gift);

                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.rsz_splash_icon)
                                    .setContentTitle(title)
                                    .setContentText(message);

                    Intent resultIntent = new Intent(context, NotificationsActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                    stackBuilder.addParentStack(NotificationsActivity.class);
                    stackBuilder.addNextIntent(resultIntent);
                    PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                    mBuilder.setContentIntent(resultPendingIntent);
                    NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
                    mBuilder.setAutoCancel(true);
                    mNotificationManager.notify(0, mBuilder.build());
                }

                break;
            }

            case GCM_NOTIFY_MESSAGE: {

                if (App.getInstance().getId() != 0 && Long.valueOf(accountId) == App.getInstance().getId()) {

                    if (App.getInstance().getCurrentChatId() == Integer.valueOf(actionId)) {

                        Intent i = new Intent(ChatFragment.BROADCAST_ACTION);
                        i.putExtra(ChatFragment.PARAM_TASK, 0);
                        i.putExtra(ChatFragment.PARAM_STATUS, ChatFragment.STATUS_START);

                        i.putExtra("msgId", Integer.valueOf(msgId));
                        i.putExtra("msgFromUserId", Long.valueOf(msgFromUserId));
                        i.putExtra("msgFromUserState", Integer.valueOf(msgFromUserState));
                        i.putExtra("msgFromUserVerify", Integer.valueOf(msgFromUserVerify));
                        i.putExtra("msgFromUserUsername", String.valueOf(msgFromUserUsername));
                        i.putExtra("msgFromUserFullname", String.valueOf(msgFromUserFullname));
                        i.putExtra("msgFromUserPhotoUrl", String.valueOf(msgFromUserPhotoUrl));
                        i.putExtra("msgMessage", String.valueOf(msgMessage));
                        i.putExtra("msgImgUrl", String.valueOf(msgImgUrl));
                        i.putExtra("msgCreateAt", Integer.valueOf(msgCreateAt));
                        i.putExtra("msgDate", String.valueOf(msgDate));
                        i.putExtra("msgTimeAgo", String.valueOf(msgTimeAgo));

                        context.sendBroadcast(i);

                    } else {

                        if (App.getInstance().getMessagesCount() == 0)
                            App.getInstance().setMessagesCount(App.getInstance().getMessagesCount() + 1);

                          List<Integer> mList=new ArrayList<>();
                        HashSet<Integer> hashSet = new HashSet<Integer>();
                        HashMap<Integer,String> groupMessage=new HashMap<Integer,String>();
                        RemoteNotification remoteNotification = new RemoteNotification();
                        //Custom Message Notification
                        Log.d("msgID=",actionId+"");

                        if(msgImgUrl.length()!=0)
                        {
                            int unicode=0x1F4F7;
                            if(msgMessage.length()!=0) {
                                msgMessage = String.valueOf(Character.toChars(unicode)) + " "+msgMessage;
                            }
                            else {
                                msgMessage = String.valueOf(Character.toChars(unicode))+" Photo";
                            }
                        }

                        groupMessage=App.getInstance().getHm();
                        groupMessage.put(Integer.valueOf(msgId),msgFromUserFullname+" "+msgFromUserFamilyname+": "+msgMessage);
                        App.getInstance().setHm(groupMessage);
                        mList=App.getInstance().getMessageList();
                        mList.add(Integer.valueOf(actionId));
                        hashSet.addAll(mList);
                        mList.clear();
                        mList.addAll(hashSet);
                        App.getInstance().setMessageList(mList);


                        if (App.getInstance().getAllowMessagesGCM() == ENABLED) {

                            int count = 0;
                            Random random = new Random();
                            int notificationId = random.nextInt(9999 - 1000) + 1000;
                            Log.d("NotificationUser=", msgFromUserFullname + msgFromUserUsername);
                            if(groupMessage.size()==1) {
                                message="new message";
                            }else{
                                message = "messages from " + mList.size() + " chats";
                            }
                            /*Spannable summaryText = new SpannableString(hm.size() + " " + message);
                            summaryText.setSpan(new BackgroundColorSpan(Color.parseColor("#FFFFFF")), 0, summaryText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);*/
                            RemoteViews remoteview = new RemoteViews(context.getPackageName(), R.layout.custome_notification);
                            remoteview.setImageViewResource(R.id.iv_notificationIcon,R.drawable.notification_icon);
                            remoteview.setTextViewText(R.id.tv_notificationTitle,title);
                            remoteview.setTextViewText(R.id.tv_message, groupMessage.size() + " " + message);
                            NotificationCompat.InboxStyle inboxStyle
                                    = new NotificationCompat.InboxStyle();
                            inboxStyle.setBigContentTitle(title);

                               for(Map.Entry  msg:groupMessage.entrySet()){
                                   /*Spannable messageColor=new SpannableString(msgFromUserFullname+" "+msgFromUserFamilyname+":"+msg.getValue());
                                   messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#FFFFFF")),0,msgFromUserFullname.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                   messageColor.setSpan(new BackgroundColorSpan(Color.parseColor("#FFFFFF")),msgFromUserFullname.length(),msgFromUserFullname.length()+msgFromUserFamilyname.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                   messageColor.setSpan(new ForegroundColorSpan(Color.parseColor("#FFFFFF")),msgFromUserFullname.length()+msgFromUserFamilyname.length(),messageColor.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
*/
                                   inboxStyle.addLine(msg.getValue()+"");

                               }
                                inboxStyle.setSummaryText(groupMessage.size() + " " + message);
                            //    Log.d("chatMessage=", mList + "");

                                NotificationCompat.Builder mBuilder =
                                        new NotificationCompat.Builder(context)
                                                .setSmallIcon(getNotificationIcon())
                                                .setColor(Color.parseColor("#4A2363"))
                                                .setContentTitle(title)
                                                .setGroup(remoteNotification.getUserNotificationGroup())
                                                .setContent(remoteview);

                                Intent resultIntent = new Intent(context, DialogsActivity.class);
                                resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                                stackBuilder.addParentStack(DialogsActivity.class);
                                stackBuilder.addNextIntent(resultIntent);
                                PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                                NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                                mBuilder.setContentIntent(resultPendingIntent);
                                mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
                                mBuilder.setAutoCancel(true);
                                mBuilder.setStyle(inboxStyle);
                                mNotificationManager.notify(remoteNotification.getUserNotificationGroup(), remoteNotification.getUserNotificationId(), mBuilder.build());
                                // sendStackNotificationIfNeeded(remoteNotification,context);
                            }
                        }
                    }

                    break;
                }


            default: {

                break;
            }
        }
    }

    private static int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.ic_action_push_notification : R.drawable.notification_icon;
    }

    private  static void sendStackNotificationIfNeeded(RemoteNotification remoteNotification,Context mContext) {
        ArrayList<StatusBarNotification> groupedNotifications = new ArrayList<>();
        NotificationManager mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        // step through all the active StatusBarNotifications and
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (StatusBarNotification sbn : mNotificationManager.getActiveNotifications()) {
                // add any previously sent notifications with a group that matches our RemoteNotification
                // and exclude any previously sent stack notifications
                if (remoteNotification.getUserNotificationGroup() != null &&
                        remoteNotification.getUserNotificationGroup().equals(sbn.getNotification().getGroup()) &&
                        sbn.getId() != RemoteNotification.TYPE_STACK) {
                    groupedNotifications.add(sbn);
                }
            }

        if (groupedNotifications.size() > 1) {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext);

            // use convenience methods on our RemoteNotification wrapper to create a title
            builder.setContentTitle(String.format("%s: %s", remoteNotification.getAppName(), remoteNotification.getErrorName()))
                    .setContentText(String.format("%d new activities", groupedNotifications.size()));

            // for every previously sent notification that met our above requirements,
            // add a new line containing its title to the inbox style notification extender
            NotificationCompat.InboxStyle inbox = new NotificationCompat.InboxStyle();
            {
                for (StatusBarNotification activeSbn : groupedNotifications) {
                    String stackNotificationLine = (String) activeSbn.getNotification().extras.get(NotificationCompat.EXTRA_TITLE);
                    if (stackNotificationLine != null) {
                        inbox.addLine(stackNotificationLine);
                    }
                }

                // the summary text will appear at the bottom of the expanded stack notification
                // we just display the same thing from above (don't forget to use string
                // resource formats!)
                inbox.setSummaryText(String.format("%d new activities", groupedNotifications.size()));
            }
            builder.setStyle(inbox);

            // make sure that our group is set the same as our most recent RemoteNotification
            // and choose to make it the group summary.
            // when this option is set to true, all previously sent/active notifications
            // in the same group will be hidden in favor of the notifcation we are creating
            builder.setGroup(remoteNotification.getUserNotificationGroup())
                    .setGroupSummary(true);

            // if the user taps the notification, it should disappear after firing its content intent
            // and we set the priority to high to avoid Doze from delaying our notifications
            builder.setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_HIGH);

            // create a unique PendingIntent using an integer request code.
            final int requestCode = (int) System.currentTimeMillis() / 1000;
            Intent resultIntent = new Intent(mContext, DialogsActivity.class);
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(mContext);
            stackBuilder.addParentStack(DialogsActivity.class);
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            builder.setContentIntent(resultPendingIntent);
            Notification stackNotification = builder.build();
            stackNotification.defaults = Notification.DEFAULT_ALL;

            // finally, deliver the notification using the group identifier as the Tag
            // and the TYPE_STACK which will cause any previously sent stack notifications
            // for this group to be updated with the contents of this built summary notification
            mNotificationManager.notify(remoteNotification.getUserNotificationGroup(), RemoteNotification.TYPE_STACK, stackNotification);
        }
    }
    }


}