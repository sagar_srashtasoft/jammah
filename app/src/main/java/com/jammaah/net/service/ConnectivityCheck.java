package com.jammaah.net.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by pankajsakariya on 17/07/17.
 */

public class ConnectivityCheck extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        final ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        Intent intent1 = new Intent("Internate_Connection");

        if (activeNetwork != null && activeNetwork.isConnected()) {
            // INTERNATE IS ON

            intent1.putExtra("isInternateOn",true);

        } else {
          // INTERNATE IS OFF
            intent1.putExtra("isInternateOn",false);

        }

       // intent.putExtra("RIDE_STATUS",rideStatus);

        context.sendBroadcast(intent1);

    }
}
