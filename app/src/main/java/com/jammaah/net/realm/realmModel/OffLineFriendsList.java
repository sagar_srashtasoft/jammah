package com.jammaah.net.realm.realmModel;

import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by pankajsakariya on 21/07/17.
 */
@RealmClass
public class OffLineFriendsList extends RealmObject {
    private long id, friendTo, friendUserId;

    private int verify;

    public String getFriendUserUsername() {
        return friendUserUsername;
    }

    public void setFriendUserUsername(String friendUserUsername) {
        this.friendUserUsername = friendUserUsername;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getFriendTo() {
        return friendTo;
    }

    public void setFriendTo(long friendTo) {
        this.friendTo = friendTo;
    }

    public long getFriendUserId() {
        return friendUserId;
    }

    public void setFriendUserId(long friendUserId) {
        this.friendUserId = friendUserId;
    }

    public int getVerify() {
        return verify;
    }

    public void setVerify(int verify) {
        this.verify = verify;
    }

    public String getFriendUserFullname() {
        return friendUserFullname;
    }

    public void setFriendUserFullname(String friendUserFullname) {
        this.friendUserFullname = friendUserFullname;
    }

    public String getFriendUserPhoto() {
        return friendUserPhoto;
    }

    public void setFriendUserPhoto(String friendUserPhoto) {
        this.friendUserPhoto = friendUserPhoto;
    }

    public String getFriendLocation() {
        return friendLocation;
    }

    public void setFriendLocation(String friendLocation) {
        this.friendLocation = friendLocation;
    }

    public String getTimeAgo() {
        return timeAgo;
    }

    public void setTimeAgo(String timeAgo) {
        this.timeAgo = timeAgo;
    }

   /* public Boolean getOnline() {
        return online;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }*/

    private String friendUserUsername, friendUserFullname, friendUserPhoto, friendLocation, timeAgo;

    //private Boolean online = false;
    public OffLineFriendsList() {


    }

    public OffLineFriendsList(JSONObject jsonData) {
        try {
            if (!jsonData.getBoolean("error")) {

                this.setId(jsonData.getLong("id"));
                this.setFriendUserId(jsonData.getLong("friendUserId"));
                   this.setVerify(jsonData.getInt("friendUserVerify"));
                this.setFriendUserUsername(jsonData.getString("friendUserUsername"));
                this.setFriendUserFullname(jsonData.getString("friendUserFullname"));
                this.setFriendUserPhoto(jsonData.getString("friendUserPhoto"));
                this.setFriendLocation(jsonData.getString("friendLocation"));
                this.setFriendTo(jsonData.getLong("friendTo"));
                this.setTimeAgo(jsonData.getString("timeAgo"));
              //  this.setOnline(jsonData.getBoolean("friendUserOnline"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
