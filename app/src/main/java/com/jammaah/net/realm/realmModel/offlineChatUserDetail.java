package com.jammaah.net.realm.realmModel;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by murtuzanalawala on 7/24/17.
 */
@RealmClass
public class offlineChatUserDetail extends RealmObject {

    public offlineChatUserDetail(){

    }
    public long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(long fromUserId) {
        this.fromUserId = fromUserId;
    }

    private long fromUserId;

    public int getChatId() {
        return chatId;
    }

    public void setChatId(int chatId) {
        this.chatId = chatId;
    }

    public int getMsgId() {
        return msgId;
    }

    public void setMsgId(int msgId) {
        this.msgId = msgId;
    }

    private int chatId;
    private int msgId;

    public int getChatConversationId() {
        return chatConversationId;
    }

    public void setChatConversationId(int chatConversationId) {
        this.chatConversationId = chatConversationId;
    }

    private int chatConversationId;

}
