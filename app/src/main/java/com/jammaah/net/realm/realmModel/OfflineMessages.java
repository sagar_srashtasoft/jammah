package com.jammaah.net.realm.realmModel;

import android.util.Log;

import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by pankajsakariya on 21/07/17.
 */
@RealmClass
public class OfflineMessages extends RealmObject {

    private long withUserId, fromUserId, toUserId;
    private int id, withUserState, newMessagesCount, createAt, withUserVerify;
    private String withUserUsername, withUserFullname, withUserPhotoUrl, timeAgo, date, lastMessage, lastMessageAgo;


    public OfflineMessages() {

    }
    public OfflineMessages(JSONObject jsonData) {

        try {

            this.setId(jsonData.getInt("id"));
            this.setFromUserId(jsonData.getLong("fromUserId"));
            this.setToUserId(jsonData.getLong("toUserId"));
            this.setWithUserId(jsonData.getLong("withUserId"));
            this.setWithUserVerify(jsonData.getInt("withUserVerify"));
            this.setWithUserState(jsonData.getInt("withUserState"));
            this.setWithUserUsername(jsonData.getString("withUserUsername"));
            this.setWithUserFullname(jsonData.getString("withUserFullname"));
            this.setWithUserPhotoUrl(jsonData.getString("withUserPhotoUrl"));
            this.setLastMessage(jsonData.getString("lastMessage"));
            this.setLastMessageAgo(jsonData.getString("lastMessageAgo"));
            this.setNewMessagesCount(jsonData.getInt("newMessagesCount"));
            this.setDate(jsonData.getString("date"));
            this.setCreateAt(jsonData.getInt("createAt"));
            this.setTimeAgo(jsonData.getString("timeAgo"));

            if (jsonData.has("withUserBlocked")) {

               // this.setBlocked(jsonData.getBoolean("withUserBlocked"));
            }

        } catch (Throwable t) {

            Log.e("Chat", "Could not parse malformed JSON: \"" + jsonData.toString() + "\"");

        } finally {

            Log.d("Chat", jsonData.toString());
        }
    }


    public String getWithUserUsername() {
        return withUserUsername;
    }

    public void setWithUserUsername(String withUserUsername) {
        this.withUserUsername = withUserUsername;
    }

    public long getWithUserId() {
        return withUserId;
    }

    public void setWithUserId(long withUserId) {
        this.withUserId = withUserId;
    }

    public long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public long getToUserId() {
        return toUserId;
    }

    public void setToUserId(long toUserId) {
        this.toUserId = toUserId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWithUserState() {
        return withUserState;
    }

    public void setWithUserState(int withUserState) {
        this.withUserState = withUserState;
    }

    public int getNewMessagesCount() {
        return newMessagesCount;
    }

    public void setNewMessagesCount(int newMessagesCount) {
        this.newMessagesCount = newMessagesCount;
    }

    public int getCreateAt() {
        return createAt;
    }

    public void setCreateAt(int createAt) {
        this.createAt = createAt;
    }

    public int getWithUserVerify() {
        return withUserVerify;
    }

    public void setWithUserVerify(int withUserVerify) {
        this.withUserVerify = withUserVerify;
    }

    public String getWithUserFullname() {
        return withUserFullname;
    }

    public void setWithUserFullname(String withUserFullname) {
        this.withUserFullname = withUserFullname;
    }

    public String getWithUserPhotoUrl() {
        return withUserPhotoUrl;
    }

    public void setWithUserPhotoUrl(String withUserPhotoUrl) {
        this.withUserPhotoUrl = withUserPhotoUrl;
    }

    public String getTimeAgo() {
        return timeAgo;
    }

    public void setTimeAgo(String timeAgo) {
        this.timeAgo = timeAgo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getLastMessageAgo() {
        return lastMessageAgo;
    }

    public void setLastMessageAgo(String lastMessageAgo) {
        this.lastMessageAgo = lastMessageAgo;
    }


}
