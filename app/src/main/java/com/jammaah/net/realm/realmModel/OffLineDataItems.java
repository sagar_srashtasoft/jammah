package com.jammaah.net.realm.realmModel;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by pankajsakariya on 18/07/17.
 */

@RealmClass
public class OffLineDataItems extends RealmObject {

    private long id, fromUserId, rePostId, groupId, groupAuthor;
    private int createAt, likesCount, commentsCount, rePostsCount, allowComments, groupAllowComments, accessMode, fromUserVerify;
    private String timeAgo, date, post, imgUrl, fromUserUsername, fromUserFullname, fromUserPhotoUrl, area, country, city, youTubeVideoImg, youTubeVideoCode, youTubeVideoUrl;
  //  private Double lat = 0.000000, lng = 0.000000;
  //  private Boolean myLike, myRePost;

    private String urlPreviewTitle, urlPreviewImage, urlPreviewLink, urlPreviewDescription;

    private String previewVideoImgUrl, videoUrl;

    private String rePostTimeAgo, rePostDate, rePostPost, rePostImgUrl, rePostFromUserUsername, rePostFromUserFullname, rePostFromUserPhotoUrl;
    private int fromRePostUserVerify, rePostRemoveAt;
    private long rePostFromUserId;

    private String reVideoUrl;

    public String getStringJson() {
        return stringJson;
    }

    public void setStringJson(String stringJson) {
        this.stringJson = stringJson;
    }

    private String stringJson;
    private String reYouTubeVideoImg, reYouTubeVideoCode, reYouTubeVideoUrl;



    public OffLineDataItems() {

    }
    public OffLineDataItems(JSONObject jsonData) {

        try {

            if (!jsonData.getBoolean("error")) {

                this.setId(jsonData.getLong("id"));
                this.setRePostId(jsonData.getLong("rePostId"));
                this.setGroupId(jsonData.getLong("groupId"));
                this.setFromUserId(jsonData.getLong("fromUserId"));
                this.setAccessMode(jsonData.getInt("accessMode"));
                this.setFromUserVerify(jsonData.getInt("fromUserVerify"));
                this.setFromUserUsername(jsonData.getString("fromUserUsername"));
                this.setFromUserFullname(jsonData.getString("fromUserFullname"));
                this.setFromUserPhotoUrl(jsonData.getString("fromUserPhoto"));
                this.setPost(jsonData.getString("post"));
                this.setImgUrl(jsonData.getString("imgUrl"));
                this.setArea(jsonData.getString("area"));
                this.setCountry(jsonData.getString("country"));
                this.setCity(jsonData.getString("city"));
                this.setAllowComments(jsonData.getInt("allowComments"));
                this.setGroupAllowComments(jsonData.getInt("groupAllowComments"));
                this.setGroupAuthor(jsonData.getLong("groupAuthor"));
                this.setCommentsCount(jsonData.getInt("commentsCount"));
                this.setLikesCount(jsonData.getInt("likesCount"));
                this.setRePostsCount(jsonData.getInt("rePostsCount"));
              //  this.setMyLike(jsonData.getBoolean("myLike"));
               // this.setMyRePost(jsonData.getBoolean("myRePost"));
                //this.setLat(jsonData.getDouble("lat"));
                //this.setLng(jsonData.getDouble("lng"));
                this.setCreateAt(jsonData.getInt("createAt"));
                this.setDate(jsonData.getString("date"));
                this.setTimeAgo(jsonData.getString("timeAgo"));

                this.setYouTubeVideoImg(jsonData.getString("YouTubeVideoImg"));
                this.setYouTubeVideoCode(jsonData.getString("YouTubeVideoCode"));
                this.setYouTubeVideoUrl(jsonData.getString("YouTubeVideoUrl"));

                this.setUrlPreviewTitle(jsonData.getString("urlPreviewTitle"));
                this.setUrlPreviewImage(jsonData.getString("urlPreviewImage"));
                this.setUrlPreviewLink(jsonData.getString("urlPreviewLink"));
                this.setUrlPreviewDescription(jsonData.getString("urlPreviewDescription"));

                this.setVideoUrl(jsonData.getString("videoUrl"));
                this.setPreviewVideoImgUrl(jsonData.getString("previewVideoImgUrl"));

                if (jsonData.has("rePost")) {

                    JSONArray rePostArray = jsonData.getJSONArray("rePost");

                    if (rePostArray.length() > 0) {

                        JSONObject rePostObj = (JSONObject) rePostArray.get(0);

                        this.setRePostFromUserId(jsonData.getLong("fromUserId"));
                        this.setRePostFromUserFullname(rePostObj.getString("fromUserFullname"));
                        this.setRePostFromUserUsername(rePostObj.getString("fromUserUsername"));
                        this.setRePostFromUserPhotoUrl(rePostObj.getString("fromUserPhoto"));
                        this.setRePostPost(rePostObj.getString("post"));
                        this.setRePostImgUrl(rePostObj.getString("imgUrl"));
                        this.setRePostTimeAgo(rePostObj.getString("timeAgo"));
                        this.setFromRePostUserVerify(rePostObj.getInt("fromUserVerify"));
                        this.setRePostFromUserId(rePostObj.getLong("fromUserId"));
                        this.setRePostRemoveAt(rePostObj.getInt("removeAt"));

                        if (rePostObj.has("videoUrl")) this.setReVideoUrl(rePostObj.getString("videoUrl"));

                        if (rePostObj.has("YouTubeVideoImg")) this.setReYouTubeVideoImg(rePostObj.getString("YouTubeVideoImg"));
                        if (rePostObj.has("YouTubeVideoCode")) this.setReYouTubeVideoCode(rePostObj.getString("YouTubeVideoCode"));
                        if (rePostObj.has("YouTubeVideoUrl")) this.setReYouTubeVideoUrl(rePostObj.getString("YouTubeVideoUrl"));

                        if (rePostObj.has("urlPreviewTitle")) this.setReUrlPreviewTitle(rePostObj.getString("urlPreviewTitle"));
                        if (rePostObj.has("urlPreviewImage")) this.setReUrlPreviewImage(rePostObj.getString("urlPreviewImage"));
                        if (rePostObj.has("urlPreviewLink")) this.setReUrlPreviewLink(rePostObj.getString("urlPreviewLink"));
                        if (rePostObj.has("urlPreviewDescription")) this.setReUrlPreviewDescription(rePostObj.getString("urlPreviewDescription"));
                    }
                }
            }

        } catch (Throwable t) {

            Log.e("Item", "Could not parse malformed JSON: \"" + jsonData.toString() + "\"");

        } finally {

            Log.d("Item", jsonData.toString());
        }
    }

    public String getReUrlPreviewTitle() {
        return reUrlPreviewTitle;
    }

    public void setReUrlPreviewTitle(String reUrlPreviewTitle) {
        this.reUrlPreviewTitle = reUrlPreviewTitle;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public long getRePostId() {
        return rePostId;
    }

    public void setRePostId(long rePostId) {
        this.rePostId = rePostId;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public long getGroupAuthor() {
        return groupAuthor;
    }

    public void setGroupAuthor(long groupAuthor) {
        this.groupAuthor = groupAuthor;
    }

    public int getCreateAt() {
        return createAt;
    }

    public void setCreateAt(int createAt) {
        this.createAt = createAt;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public int getRePostsCount() {
        return rePostsCount;
    }

    public void setRePostsCount(int rePostsCount) {
        this.rePostsCount = rePostsCount;
    }

    public int getAllowComments() {
        return allowComments;
    }

    public void setAllowComments(int allowComments) {
        this.allowComments = allowComments;
    }

    public int getGroupAllowComments() {
        return groupAllowComments;
    }

    public void setGroupAllowComments(int groupAllowComments) {
        this.groupAllowComments = groupAllowComments;
    }

    public int getAccessMode() {
        return accessMode;
    }

    public void setAccessMode(int accessMode) {
        this.accessMode = accessMode;
    }

    public int getFromUserVerify() {
        return fromUserVerify;
    }

    public void setFromUserVerify(int fromUserVerify) {
        this.fromUserVerify = fromUserVerify;
    }

    public String getTimeAgo() {
        return timeAgo;
    }

    public void setTimeAgo(String timeAgo) {
        this.timeAgo = timeAgo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getFromUserUsername() {
        return fromUserUsername;
    }

    public void setFromUserUsername(String fromUserUsername) {
        this.fromUserUsername = fromUserUsername;
    }

    public String getFromUserFullname() {
        return fromUserFullname;
    }

    public void setFromUserFullname(String fromUserFullname) {
        this.fromUserFullname = fromUserFullname;
    }

    public String getFromUserPhotoUrl() {
        return fromUserPhotoUrl;
    }

    public void setFromUserPhotoUrl(String fromUserPhotoUrl) {
        this.fromUserPhotoUrl = fromUserPhotoUrl;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getYouTubeVideoImg() {
        return youTubeVideoImg;
    }

    public void setYouTubeVideoImg(String youTubeVideoImg) {
        this.youTubeVideoImg = youTubeVideoImg;
    }

    public String getYouTubeVideoCode() {
        return youTubeVideoCode;
    }

    public void setYouTubeVideoCode(String youTubeVideoCode) {
        this.youTubeVideoCode = youTubeVideoCode;
    }

    public String getYouTubeVideoUrl() {
        return youTubeVideoUrl;
    }

    public void setYouTubeVideoUrl(String youTubeVideoUrl) {
        this.youTubeVideoUrl = youTubeVideoUrl;
    }

 /*   public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }*/

   /* public Boolean getMyLike() {
        return myLike;
    }

    public void setMyLike(Boolean myLike) {
        this.myLike = myLike;
    }

    public Boolean getMyRePost() {
        return myRePost;
    }

    public void setMyRePost(Boolean myRePost) {
        this.myRePost = myRePost;
    }*/

    public String getUrlPreviewTitle() {
        return urlPreviewTitle;
    }

    public void setUrlPreviewTitle(String urlPreviewTitle) {
        this.urlPreviewTitle = urlPreviewTitle;
    }

    public String getUrlPreviewImage() {
        return urlPreviewImage;
    }

    public void setUrlPreviewImage(String urlPreviewImage) {
        this.urlPreviewImage = urlPreviewImage;
    }

    public String getUrlPreviewLink() {
        return urlPreviewLink;
    }

    public void setUrlPreviewLink(String urlPreviewLink) {
        this.urlPreviewLink = urlPreviewLink;
    }

    public String getUrlPreviewDescription() {
        return urlPreviewDescription;
    }

    public void setUrlPreviewDescription(String urlPreviewDescription) {
        this.urlPreviewDescription = urlPreviewDescription;
    }

    public String getPreviewVideoImgUrl() {
        return previewVideoImgUrl;
    }

    public void setPreviewVideoImgUrl(String previewVideoImgUrl) {
        this.previewVideoImgUrl = previewVideoImgUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getRePostTimeAgo() {
        return rePostTimeAgo;
    }

    public void setRePostTimeAgo(String rePostTimeAgo) {
        this.rePostTimeAgo = rePostTimeAgo;
    }

    public String getRePostDate() {
        return rePostDate;
    }

    public void setRePostDate(String rePostDate) {
        this.rePostDate = rePostDate;
    }

    public String getRePostPost() {
        return rePostPost;
    }

    public void setRePostPost(String rePostPost) {
        this.rePostPost = rePostPost;
    }

    public String getRePostImgUrl() {
        return rePostImgUrl;
    }

    public void setRePostImgUrl(String rePostImgUrl) {
        this.rePostImgUrl = rePostImgUrl;
    }

    public String getRePostFromUserUsername() {
        return rePostFromUserUsername;
    }

    public void setRePostFromUserUsername(String rePostFromUserUsername) {
        this.rePostFromUserUsername = rePostFromUserUsername;
    }

    public String getRePostFromUserFullname() {
        return rePostFromUserFullname;
    }

    public void setRePostFromUserFullname(String rePostFromUserFullname) {
        this.rePostFromUserFullname = rePostFromUserFullname;
    }

    public String getRePostFromUserPhotoUrl() {
        return rePostFromUserPhotoUrl;
    }

    public void setRePostFromUserPhotoUrl(String rePostFromUserPhotoUrl) {
        this.rePostFromUserPhotoUrl = rePostFromUserPhotoUrl;
    }

    public int getFromRePostUserVerify() {
        return fromRePostUserVerify;
    }

    public void setFromRePostUserVerify(int fromRePostUserVerify) {
        this.fromRePostUserVerify = fromRePostUserVerify;
    }

    public int getRePostRemoveAt() {
        return rePostRemoveAt;
    }

    public void setRePostRemoveAt(int rePostRemoveAt) {
        this.rePostRemoveAt = rePostRemoveAt;
    }

    public long getRePostFromUserId() {
        return rePostFromUserId;
    }

    public void setRePostFromUserId(long rePostFromUserId) {
        this.rePostFromUserId = rePostFromUserId;
    }

    public String getReVideoUrl() {
        return reVideoUrl;
    }

    public void setReVideoUrl(String reVideoUrl) {
        this.reVideoUrl = reVideoUrl;
    }

    public String getReYouTubeVideoImg() {
        return reYouTubeVideoImg;
    }

    public void setReYouTubeVideoImg(String reYouTubeVideoImg) {
        this.reYouTubeVideoImg = reYouTubeVideoImg;
    }

    public String getReYouTubeVideoCode() {
        return reYouTubeVideoCode;
    }

    public void setReYouTubeVideoCode(String reYouTubeVideoCode) {
        this.reYouTubeVideoCode = reYouTubeVideoCode;
    }

    public String getReYouTubeVideoUrl() {
        return reYouTubeVideoUrl;
    }

    public void setReYouTubeVideoUrl(String reYouTubeVideoUrl) {
        this.reYouTubeVideoUrl = reYouTubeVideoUrl;
    }

    public String getReUrlPreviewImage() {
        return reUrlPreviewImage;
    }

    public void setReUrlPreviewImage(String reUrlPreviewImage) {
        this.reUrlPreviewImage = reUrlPreviewImage;
    }

    public String getReUrlPreviewLink() {
        return reUrlPreviewLink;
    }

    public void setReUrlPreviewLink(String reUrlPreviewLink) {
        this.reUrlPreviewLink = reUrlPreviewLink;
    }

    public String getReUrlPreviewDescription() {
        return reUrlPreviewDescription;
    }

    public void setReUrlPreviewDescription(String reUrlPreviewDescription) {
        this.reUrlPreviewDescription = reUrlPreviewDescription;
    }

    public int getAd() {
        return ad;
    }

    public void setAd(int ad) {
        this.ad = ad;
    }

    private String reUrlPreviewTitle, reUrlPreviewImage, reUrlPreviewLink, reUrlPreviewDescription;

    private int ad = 0;


}
