package com.jammaah.net.realm.realmModel;

import android.util.Log;

import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by pankajsakariya on 21/07/17.
 */
@RealmClass
public class OfflineProfileData extends RealmObject {

    private long id;

    private int state, sex, year, month, day, verify, itemsCount, photosCount, videosCount, likesCount, giftsCount, friendsCount, followingsCount, followersCount, allowComments, allowMessages, lastAuthorize, accountType;

    private int allowShowMyInfo, allowShowMyVideos, allowShowMyFriends, allowShowMyPhotos, allowShowMyGifts;

    private double distance = 0;

    private String username, fullname, lowPhotoUrl, bigPhotoUrl, normalPhotoUrl, normalCoverUrl, location, facebookPage, instagramPage, bio, lastAuthorizeDate, lastAuthorizeTimeAgo;

    public String getFamilyname() {
        return familyname;
    }

    public void setFamilyname(String familyname) {
        this.familyname = familyname;
    }

    private String familyname;

    public OfflineProfileData() {


    }
    public OfflineProfileData(JSONObject jsonData) {

        try {

            if (!jsonData.getBoolean("error")) {

                this.setId(jsonData.getLong("id"));
                this.setState(jsonData.getInt("state"));
                this.setAccountType(jsonData.getInt("accountType"));

                this.setSex(jsonData.getInt("sex"));
                this.setYear(jsonData.getInt("year"));
                this.setMonth(jsonData.getInt("month"));
                this.setDay(jsonData.getInt("day"));
                this.setUsername(jsonData.getString("username"));
                this.setFullname(jsonData.getString("fullname"));
                this.setFamilyname(jsonData.getString("familyname"));
                this.setLocation(jsonData.getString("location"));
                this.setFacebookPage(jsonData.getString("fb_page"));
                this.setInstagramPage(jsonData.getString("instagram_page"));
                this.setBio(jsonData.getString("status"));
                this.setVerify(jsonData.getInt("verify"));

                this.setLowPhotoUrl(jsonData.getString("lowPhotoUrl"));
                this.setNormalPhotoUrl(jsonData.getString("normalPhotoUrl"));
                this.setBigPhotoUrl(jsonData.getString("bigPhotoUrl"));

                this.setNormalCoverUrl(jsonData.getString("normalCoverUrl"));

                this.setFollowersCount(jsonData.getInt("followersCount"));
                this.setFollowingsCount(jsonData.getInt("friendsCount"));
                this.setFriendsCount(jsonData.getInt("friendsCount"));
                this.setItemsCount(jsonData.getInt("postsCount"));
                this.setLikesCount(jsonData.getInt("likesCount"));
                this.setPhotosCount(jsonData.getInt("photosCount"));
                this.setGiftsCount(jsonData.getInt("giftsCount"));
                this.setVideosCount(jsonData.getInt("videosCount"));

                this.setAllowShowMyInfo(jsonData.getInt("allowShowMyInfo"));
                this.setAllowShowMyVideos(jsonData.getInt("allowShowMyVideos"));
                this.setAllowShowMyFriends(jsonData.getInt("allowShowMyFriends"));
                this.setAllowShowMyPhotos(jsonData.getInt("allowShowMyPhotos"));
                this.setAllowShowMyGifts(jsonData.getInt("allowShowMyGifts"));

                this.setAllowComments(jsonData.getInt("allowComments"));
                this.setAllowMessages(jsonData.getInt("allowMessages"));

                //this.setInBlackList(jsonData.getBoolean("inBlackList"));
                //this.setFollower(jsonData.getBoolean("follower"));
                //this.setFollow(jsonData.getBoolean("follow"));
               // this.setFriend(jsonData.getBoolean("friend"));
             //   this.setOnline(jsonData.getBoolean("online"));
              //  this.setBlocked(jsonData.getBoolean("blocked"));

                this.setLastAuthorize(jsonData.getInt("lastAuthorize"));
                this.setLastAuthorizeDate(jsonData.getString("lastAuthorizeDate"));
                this.setLastAuthorizeTimeAgo(jsonData.getString("lastAuthorizeTimeAgo"));

                if (jsonData.has("distance")) {

                    this.setDistance(jsonData.getDouble("distance"));
                }
            }

        } catch (Throwable t) {

            Log.e("Profile", "Could not parse malformed JSON: \"" + jsonData.toString() + "\"");

        } finally {

            Log.d("Profile", jsonData.toString());
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getVerify() {
        return verify;
    }

    public void setVerify(int verify) {
        this.verify = verify;
    }

    public int getItemsCount() {
        return itemsCount;
    }

    public void setItemsCount(int itemsCount) {
        this.itemsCount = itemsCount;
    }

    public int getPhotosCount() {
        return photosCount;
    }

    public void setPhotosCount(int photosCount) {
        this.photosCount = photosCount;
    }

    public int getVideosCount() {
        return videosCount;
    }

    public void setVideosCount(int videosCount) {
        this.videosCount = videosCount;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public int getGiftsCount() {
        return giftsCount;
    }

    public void setGiftsCount(int giftsCount) {
        this.giftsCount = giftsCount;
    }

    public int getFriendsCount() {
        return friendsCount;
    }

    public void setFriendsCount(int friendsCount) {
        this.friendsCount = friendsCount;
    }

    public int getFollowingsCount() {
        return followingsCount;
    }

    public void setFollowingsCount(int followingsCount) {
        this.followingsCount = followingsCount;
    }

    public int getFollowersCount() {
        return followersCount;
    }

    public void setFollowersCount(int followersCount) {
        this.followersCount = followersCount;
    }

    public int getAllowComments() {
        return allowComments;
    }

    public void setAllowComments(int allowComments) {
        this.allowComments = allowComments;
    }

    public int getAllowMessages() {
        return allowMessages;
    }

    public void setAllowMessages(int allowMessages) {
        this.allowMessages = allowMessages;
    }

    public int getLastAuthorize() {
        return lastAuthorize;
    }

    public void setLastAuthorize(int lastAuthorize) {
        this.lastAuthorize = lastAuthorize;
    }

    public int getAccountType() {
        return accountType;
    }

    public void setAccountType(int accountType) {
        this.accountType = accountType;
    }

    public int getAllowShowMyInfo() {
        return allowShowMyInfo;
    }

    public void setAllowShowMyInfo(int allowShowMyInfo) {
        this.allowShowMyInfo = allowShowMyInfo;
    }

    public int getAllowShowMyVideos() {
        return allowShowMyVideos;
    }

    public void setAllowShowMyVideos(int allowShowMyVideos) {
        this.allowShowMyVideos = allowShowMyVideos;
    }

    public int getAllowShowMyFriends() {
        return allowShowMyFriends;
    }

    public void setAllowShowMyFriends(int allowShowMyFriends) {
        this.allowShowMyFriends = allowShowMyFriends;
    }

    public int getAllowShowMyPhotos() {
        return allowShowMyPhotos;
    }

    public void setAllowShowMyPhotos(int allowShowMyPhotos) {
        this.allowShowMyPhotos = allowShowMyPhotos;
    }

    public int getAllowShowMyGifts() {
        return allowShowMyGifts;
    }

    public void setAllowShowMyGifts(int allowShowMyGifts) {
        this.allowShowMyGifts = allowShowMyGifts;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getLowPhotoUrl() {
        return lowPhotoUrl;
    }

    public void setLowPhotoUrl(String lowPhotoUrl) {
        this.lowPhotoUrl = lowPhotoUrl;
    }

    public String getBigPhotoUrl() {
        return bigPhotoUrl;
    }

    public void setBigPhotoUrl(String bigPhotoUrl) {
        this.bigPhotoUrl = bigPhotoUrl;
    }

    public String getNormalPhotoUrl() {
        return normalPhotoUrl;
    }

    public void setNormalPhotoUrl(String normalPhotoUrl) {
        this.normalPhotoUrl = normalPhotoUrl;
    }

    public String getNormalCoverUrl() {
        return normalCoverUrl;
    }

    public void setNormalCoverUrl(String normalCoverUrl) {
        this.normalCoverUrl = normalCoverUrl;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getFacebookPage() {
        return facebookPage;
    }

    public void setFacebookPage(String facebookPage) {
        this.facebookPage = facebookPage;
    }

    public String getInstagramPage() {
        return instagramPage;
    }

    public void setInstagramPage(String instagramPage) {
        this.instagramPage = instagramPage;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getLastAuthorizeDate() {
        return lastAuthorizeDate;
    }

    public void setLastAuthorizeDate(String lastAuthorizeDate) {
        this.lastAuthorizeDate = lastAuthorizeDate;
    }

    public String getLastAuthorizeTimeAgo() {
        return lastAuthorizeTimeAgo;
    }

    public void setLastAuthorizeTimeAgo(String lastAuthorizeTimeAgo) {
        this.lastAuthorizeTimeAgo = lastAuthorizeTimeAgo;
    }


}
