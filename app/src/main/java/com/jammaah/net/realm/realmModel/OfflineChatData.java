package com.jammaah.net.realm.realmModel;

import android.util.Log;

import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by murtuzanalawala on 7/24/17.
 */
@RealmClass
public class OfflineChatData extends RealmObject {
    private long fromUserId;
    private int id, fromUserState, fromUserVerify, createAt, listId = 0;
    private String fromUserUsername;
    private String fromUserFullname;
    private String fromFamilyname;
    private String fromUserPhotoUrl;
    private String message;
    private String imgUrl;
    private String timeAgo;

    public int getChatConversationId() {
        return chatConversationId;
    }

    public void setChatConversationId(int chatConversationId) {
        this.chatConversationId = chatConversationId;
    }

    private int chatConversationId;


    public OfflineChatData() {

    }

    public OfflineChatData(JSONObject jsonData) {

        try {

            this.setId(jsonData.getInt("id"));
            this.setFromUserId(jsonData.getLong("fromUserId"));
            this.setFromUserState(jsonData.getInt("fromUserState"));
            this.setFromUserVerify(jsonData.getInt("fromUserVerify"));
            this.setFromUserUsername(jsonData.getString("fromUserUsername"));
            this.setFromUserFullname(jsonData.getString("fromUserFullname"));
            this.setFromFamilyname(jsonData.getString("fromUserFamilyname"));
            this.setFromUserPhotoUrl(jsonData.getString("fromUserPhotoUrl"));
            this.setMessage(jsonData.getString("message"));
            this.setImgUrl(jsonData.getString("imgUrl"));
            this.setCreateAt(jsonData.getInt("createAt"));
            this.setDate(jsonData.getString("date"));
            this.setTimeAgo(jsonData.getString("timeAgo"));

        } catch (Throwable t) {

            Log.e("ChatItem", "Could not parse malformed JSON: \"" + jsonData.toString() + "\"");

        } finally {

            Log.d("ChatItem", jsonData.toString());
        }
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFromUserState() {
        return fromUserState;
    }

    public void setFromUserState(int fromUserState) {
        this.fromUserState = fromUserState;
    }

    public int getFromUserVerify() {
        return fromUserVerify;
    }

    public void setFromUserVerify(int fromUserVerify) {
        this.fromUserVerify = fromUserVerify;
    }

    public int getCreateAt() {
        return createAt;
    }

    public void setCreateAt(int createAt) {
        this.createAt = createAt;
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public String getFromUserUsername() {
        return fromUserUsername;
    }

    public void setFromUserUsername(String fromUserUsername) {
        this.fromUserUsername = fromUserUsername;
    }

    public String getFromUserFullname() {
        return fromUserFullname;
    }

    public void setFromUserFullname(String fromUserFullname) {
        this.fromUserFullname = fromUserFullname;
    }

    public String getFromFamilyname() {
        return fromFamilyname;
    }

    public void setFromFamilyname(String fromFamilyname) {
        this.fromFamilyname = fromFamilyname;
    }

    public String getFromUserPhotoUrl() {
        return fromUserPhotoUrl;
    }

    public void setFromUserPhotoUrl(String fromUserPhotoUrl) {
        this.fromUserPhotoUrl = fromUserPhotoUrl;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTimeAgo() {
        return timeAgo;
    }

    public void setTimeAgo(String timeAgo) {
        this.timeAgo = timeAgo;
    }

    private String date;
}
