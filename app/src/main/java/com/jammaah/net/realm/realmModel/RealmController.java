package com.jammaah.net.realm.realmModel;

import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by pankajsakariya on 18/07/17.
 */

public class RealmController {

    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }

    public Realm getRealm() {

        return realm;
    }

    //Refresh the realm istance
    public void refresh() {

        realm.refresh();
    }

    //clear all objects from OffLineDataItems.class
    public void clearAllPost() {

        realm.beginTransaction();
        realm.clear(OffLineDataItems.class);
        realm.commitTransaction();
    }
    //clear all objects from OffLineFriendsList.class
    public void clearFriendList(){
        realm.beginTransaction();
        realm.clear(OffLineFriendsList.class);
        realm.commitTransaction();
    }

    //clear all objects from OfflineStrem.class
    public void clearAllStremList(){
        realm.beginTransaction();
        realm.clear(OfflineStrem.class);
        realm.commitTransaction();
    }

    //clear all objects from OffLineNotificationData.class
    public void clearAllNotificationList(){
        realm.beginTransaction();
        realm.clear(OffLineNotificationData.class);
        realm.commitTransaction();
    }

    //clear all objects from OfflineProfileData.class
    public void clearAllProfileData(){
        realm.beginTransaction();
        realm.clear(OfflineProfileData.class);
        realm.commitTransaction();
    }
    //clear all objects from OfflineMessages.class
    public void clearAllMessagesData(){
        realm.beginTransaction();
        realm.clear(OfflineMessages.class);
        realm.commitTransaction();
    }

    //clear all objects from OffLineDataItems.class
    public void clearProfileItem() {

        realm.beginTransaction();
        realm.clear(OfflineProfileItem.class);
        realm.commitTransaction();
    }

    //clear all objects from OffLineDataItems.class
    public void clearUserChatData() {

        realm.beginTransaction();
        realm.clear(OfflineChatData.class);
        realm.commitTransaction();
    }

    //clear all objects from OffLineDataItems.class
    public void clearChatUser() {

        realm.beginTransaction();
        realm.clear(offlineChatUserDetail.class);
        realm.commitTransaction();
    }

    //find all objects in the OffLineDataItems.class
    public RealmResults<OffLineDataItems> getOffLineItem() {

        return realm.where(OffLineDataItems.class).findAll();
    }

    //find all objects in the OffLineFriendsList.class
    public RealmResults<OffLineFriendsList> getOffLineFriendsList() {

        return realm.where(OffLineFriendsList.class).findAll();
    }

    //find all objects in the OfflineStrem.class
    public RealmResults<OfflineStrem> getOffLineStrem() {

        return realm.where(OfflineStrem.class).findAll();
    }

    //find all objects in the OffLineNotificationData.class
    public RealmResults<OffLineNotificationData> getOfflineNotification() {

        return realm.where(OffLineNotificationData.class).findAll();
    }

    //find all objects in the OfflineProfileData.class
    public RealmResults<OfflineProfileData> getOfflineProfileData() {

        return realm.where(OfflineProfileData.class).findAll();
    }

    //find all objects in the OfflineMessages.class
    public RealmResults<OfflineMessages> getOfflineMessages() {

        return realm.where(OfflineMessages.class).findAll();
    }
    //find all objects in the OfflineMessages.class
    public RealmResults<OfflineProfileItem> getOfflineProfileItem() {

        return realm.where(OfflineProfileItem.class).findAll();
    }

    //find all objects in the OfflineMessages.class
    public RealmResults<OfflineChatData> getOfflineUserChatData() {

        return realm.where(OfflineChatData.class).findAll();
    }

    //query a single item with the given id
    public OffLineDataItems getBook(String id) {

        return realm.where(OffLineDataItems.class).equalTo("id", id).findFirst();
    }

    //find all objects in the OffLineDataItems.class
    public RealmResults<offlineChatUserDetail> getChatUserDetail() {

        return realm.where(offlineChatUserDetail.class).findAll();
    }

    //find all objects in the OffLineDataItems.class
    public RealmResults<offlineChatUserDetail> getChatUserFromId(long fromID) {

        return realm.where(offlineChatUserDetail.class).equalTo("fromUserId",fromID).findAll();
        //return null;
    }
    //find all objects in the OffLineDataItems.class
    public void deleteChatUserFromId(long fromID) {

         realm.where(offlineChatUserDetail.class).equalTo("fromUserId",fromID).findFirst().removeFromRealm();

       // return null;
    }

   public void deleteChatConversion(int ChatConversionId){
       realm.where(offlineChatUserDetail.class).equalTo("fromUserId",ChatConversionId).findAll().clear();

   }

    //check if Book.class is empty
    public boolean hasBooks() {

        return !realm.allObjects(OffLineDataItems.class).isEmpty();
    }

    //query example
    public RealmResults<OffLineDataItems> queryedBooks() {

        return realm.where(OffLineDataItems.class)
                .contains("author", "Author 0")
                .or()
                .contains("title", "Realm")
                .findAll();

    }
}
