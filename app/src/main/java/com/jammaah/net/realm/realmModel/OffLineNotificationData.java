package com.jammaah.net.realm.realmModel;

import android.util.Log;

import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by pankajsakariya on 21/07/17.
 */
@RealmClass
public class OffLineNotificationData extends RealmObject {

    private long id;

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public int getFromUserState() {
        return fromUserState;
    }

    public void setFromUserState(int fromUserState) {
        this.fromUserState = fromUserState;
    }

    public int getCreateAt() {
        return createAt;
    }

    public void setCreateAt(int createAt) {
        this.createAt = createAt;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getActionStatus() {
        return actionStatus;
    }

    public void setActionStatus(int actionStatus) {
        this.actionStatus = actionStatus;
    }

    public int getUntakenNotification() {
        return untakenNotification;
    }

    public void setUntakenNotification(int untakenNotification) {
        this.untakenNotification = untakenNotification;
    }

    public String getFromUserUsername() {
        return fromUserUsername;
    }

    public void setFromUserUsername(String fromUserUsername) {
        this.fromUserUsername = fromUserUsername;
    }

    public String getFromUserFullname() {
        return fromUserFullname;
    }

    public void setFromUserFullname(String fromUserFullname) {
        this.fromUserFullname = fromUserFullname;
    }

    public String getFromUserPhotoUrl() {
        return fromUserPhotoUrl;
    }

    public void setFromUserPhotoUrl(String fromUserPhotoUrl) {
        this.fromUserPhotoUrl = fromUserPhotoUrl;
    }

    public String getTimeAgo() {
        return timeAgo;
    }

    public void setTimeAgo(String timeAgo) {
        this.timeAgo = timeAgo;
    }

    private long itemId;
    private long fromUserId;
    private int fromUserState;
    private int createAt;
    private int type;
    private int actionStatus;
    private int untakenNotification=0;
    private String fromUserUsername, fromUserFullname, fromUserPhotoUrl, timeAgo;
    public OffLineNotificationData() {

    }

    public OffLineNotificationData(JSONObject jsonData) {

        try {

            this.setId(jsonData.getLong("id"));
            this.setType(jsonData.getInt("type"));
            this.setItemId(jsonData.getLong("postId"));
            //Add Action taken status
            this.setActionStatus(jsonData.getInt("actionstatus"));
            this.setFromUserId(jsonData.getLong("fromUserId"));
            this.setFromUserState(jsonData.getInt("fromUserState"));
            this.setFromUserUsername(jsonData.getString("fromUserUsername"));
            this.setFromUserFullname(jsonData.getString("fromUserFullname"));
            this.setFromUserPhotoUrl(jsonData.getString("fromUserPhotoUrl"));
            this.setTimeAgo(jsonData.getString("timeAgo"));
            this.setCreateAt(jsonData.getInt("createAt"));

        } catch (Throwable t) {

            Log.e("Notify", "Could not parse malformed JSON: \"" + jsonData.toString() + "\"");

        } finally {

            Log.d("Notify", jsonData.toString());
        }
    }
}
