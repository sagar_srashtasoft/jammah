package com.jammaah.net;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jammaah.net.app.App;
import com.jammaah.net.common.ActivityBase;
import com.jammaah.net.dialogs.CoverChooseDialog;
import com.jammaah.net.dialogs.FriendRequestActionDialog;
import com.jammaah.net.dialogs.MyPhotoActionDialog;
import com.jammaah.net.dialogs.MyPostActionDialog;
import com.jammaah.net.dialogs.PeopleNearbySettingsDialog;
import com.jammaah.net.dialogs.PhotoChooseDialog;
import com.jammaah.net.dialogs.PhotoDeleteDialog;
import com.jammaah.net.dialogs.PostActionDialog;
import com.jammaah.net.dialogs.PostDeleteDialog;
import com.jammaah.net.dialogs.PostReportDialog;
import com.jammaah.net.dialogs.PostShareDialog;
import com.jammaah.net.dialogs.ProfileBlockDialog;
import com.jammaah.net.dialogs.ProfileReportDialog;
import com.jammaah.net.dialogs.SearchSettingsDialog;
import com.jammaah.net.model.Notify;
import com.jammaah.net.model.Profile;
import com.jammaah.net.util.CheckInternet;
import com.jammaah.net.util.UpdateUI;
import com.jammaah.net.view.ResizableImageView;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MainActivity extends ActivityBase implements FragmentDrawer.FragmentDrawerListener,Notify.NotificationCountChange,App.FamilystageListener, PhotoChooseDialog.AlertPositiveListener, CoverChooseDialog.AlertPositiveListener, ProfileReportDialog.AlertPositiveListener, ProfileBlockDialog.AlertPositiveListener, PostDeleteDialog.AlertPositiveListener, PostReportDialog.AlertPositiveListener, MyPostActionDialog.AlertPositiveListener, PostActionDialog.AlertPositiveListener, PostShareDialog.AlertPositiveListener, PhotoDeleteDialog.AlertPositiveListener, MyPhotoActionDialog.AlertPositiveListener, PeopleNearbySettingsDialog.AlertPositiveListener, SearchSettingsDialog.AlertPositiveListener, FriendRequestActionDialog.AlertPositiveListener ,UpdateUI{

    Toolbar mToolbar;
    //store family Name is empty status
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    //use for AppIntroSlider
    private ViewPager viewPager;
    private LinearLayout dotsLayout,familyemptyTag,internatePopup;
    private Button AppIntroOk;
    private MyViewPagerAdapter myViewPagerAdapter;
    private int[] layouts;
    private TextView[] dots;
    private FragmentDrawer drawerFragment;
    private boolean AppIntroFlag=false;
    // used to store app title
    private CharSequence mTitle;
    private String familyTag="false";
    LinearLayout mContainerAdmob;
    private static final int PROFILE_EDIT = 3;
    Fragment fragment;
    Boolean action = false;
    int page = 0;
    Notify notify =new Notify();
    public showingConnectivity showingConnectivity;
    private Boolean restore = false;
    UpdateUI updateUI;
    ImageView iv_closeinternetBanner;

    public static InternetListner minternetlister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        if (savedInstanceState != null) {

            //Restore the fragment's instance
            fragment = getSupportFragmentManager().getFragment(savedInstanceState, "currentFragment");

            restore = savedInstanceState.getBoolean("restore");
            mTitle = savedInstanceState.getString("mTitle");

        } else {

            fragment = new FeedFragment();

            restore = false;
            mTitle = getString(R.string.app_name);
        }

        if (fragment != null) {

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.container_body, fragment).commit();
        }

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        sharedPreferences = getApplicationContext().getSharedPreferences("MysharedPref", MODE_PRIVATE);
        editor=sharedPreferences.edit();
        notify.setNotificationCountChange(this);
        App.getInstance().setFamilystageListener(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(mTitle);

        drawerFragment = (FragmentDrawer) getFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);

        mContainerAdmob = (LinearLayout) findViewById(R.id.container_admob);
        familyemptyTag=(LinearLayout)findViewById(R.id.familyPopup);
        internatePopup = (LinearLayout)findViewById(R.id.internatePopup);
        iv_closeinternetBanner  = (ImageView)findViewById(R.id.iv_closeinternetBanner);
        mContainerAdmob.setVisibility(View.GONE);
        updateUI = this;
        //Register Brodcast
        if(showingConnectivity == null){
            showingConnectivity = new showingConnectivity();

        }
        registerReceiver(showingConnectivity,new IntentFilter("Internate_Connection"));

       //When Family NAme Empty then Display Fill family name Tag

        if(App.getInstance().getFamilyStatus())
        {
            familyemptyTag.setVisibility(View.GONE);

        }
        else {
            familyemptyTag.setVisibility(View.VISIBLE);
        }
        iv_closeinternetBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                internatePopup.setVisibility(View.GONE);
            }
        });

        familyemptyTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Profile profile=new Profile();
              //  Log.d("profiledetails=",profile.getFullname());
               Intent intent=new Intent(MainActivity.this,AccountSettingsActivity.class);
                intent.putExtra("profileId", App.getInstance().getId());
                intent.putExtra("sex",App.getInstance().getSex());
                intent.putExtra("year", App.getInstance().getYear());
                intent.putExtra("month", App.getInstance().getMonth());
                intent.putExtra("day", App.getInstance().getDay());
                intent.putExtra("fullname",App.getInstance().getFullname());
                intent.putExtra("location", App.getInstance().getLoc());
                intent.putExtra("facebookPage", App.getInstance().getFacebookPage());
                intent.putExtra("instagramPage", App.getInstance().getInstagramPage());
                intent.putExtra("bio", App.getInstance().getBio());
                startActivity(intent);
            }
        });

        if (!restore) {

            // 3 FEED SECTION
            // 4 STREAM SECTION
            // 5 POPULAR SECTION
            // 6 NOTIFICATIONS SECTION
            // 7 MESSAGES SECTION

            // FOR OTHERS INDEXES: SEE displayView FUNCTION

            displayView(3);
        }

        //Big style Notification
        try {
            final String message=this.getIntent().getStringExtra("message");
            if(!(message.equals(""))||!(message.equals(null)))
            {
                new Handler().postDelayed(new Runnable() {


                    @Override
                    public void run() {

                        final Dialog instruction = new Dialog(MainActivity.this);
                        instruction.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        instruction.setContentView(R.layout.notification_popup);
                        TextView adminMessage=(TextView)instruction.findViewById(R.id.tv_notificationMessage);
                        TextView postlink = (TextView) instruction.findViewById(R.id.tv_postLink);
                        ResizableImageView splash_popup=(ResizableImageView)instruction.findViewById(R.id.lv_notification_popupimage);
                        Button instruction_ok = (Button) instruction.findViewById(R.id.btn_notification_popup);


                        if(message.contains("http"))
                        {
                               postlink.setText(message);
                               Linkify.addLinks(postlink,Linkify.ALL);
                        }

                       instruction_ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                instruction.dismiss();
                            }
                        });
                        instruction.show();

                    }
                }, 2000);
            }

        }catch(Exception ex){
            ex.printStackTrace();
        }

        //Use For AppIntroSlider
        try {
            if (this.getIntent().getStringExtra("AppIntro").equalsIgnoreCase("AppIntroDialog")) {
                AppIntroFlag = true;

            }

        }catch (Exception ex)
        {
            ex.printStackTrace();
        }
            new Handler().postDelayed(new Runnable() {


                @Override
                public void run() {
                    if(AppIntroFlag) {
                        AppIntro();
                        AppIntroFlag=false;

                    }

                }
            }, 2000);

    }

    //Add slide to Dialog for AppIntroSlider
    private void AppIntro() {
        final Dialog AppIntroSlider=new Dialog(MainActivity.this);
        AppIntroSlider.requestWindowFeature(Window.FEATURE_NO_TITLE);
        AppIntroSlider.setContentView(R.layout.appintroslider);

        viewPager = (ViewPager) AppIntroSlider.findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) AppIntroSlider.findViewById(R.id.layoutDots);
        AppIntroOk=(Button)AppIntroSlider.findViewById(R.id.btn_AppIntrOk);
        layouts = new int[]{
                R.layout.appintro_first_layout,
                R.layout.appintro_third_layout,
                R.layout.appintro_second_layout,
        };
        addBottomDots(0);
        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
        AppIntroOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              AppIntroSlider.dismiss();
            }
        });
        AppIntroSlider.show();

    }
    private void addBottomDots(int curentPage) {
        dots = new TextView[layouts.length];
        int[] colorActive=getResources().getIntArray(R.array.array_dot_active);
        int[] colorInActive=getResources().getIntArray(R.array.array_dot_inactive);
        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorInActive[curentPage]);
            dotsLayout.addView(dots[i]);
        }
        if (dots.length > 0)
            dots[curentPage].setTextColor(colorActive[curentPage]);
    }
    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

            // changing the next button text 'NEXT' / 'GOT IT'

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    @Override
    public void onCountChanged(int untakenCount) {
        Log.d("untakenChange=",untakenCount+"");
        if(page==7)
        {
            getSupportActionBar().setTitle("Notifications "+"("+untakenCount+")");
        }
    }



    @Override
    public void OnFamilyTagChange(boolean familytagcahnge) {
        if(familytagcahnge)
        {
            familyemptyTag.setVisibility(View.GONE);

        }
        else {
            familyemptyTag.setVisibility(View.VISIBLE);
        }
    }



    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);

        outState.putBoolean("restore", true);
        outState.putString("mTitle", getSupportActionBar().getTitle().toString());
        getSupportFragmentManager().putFragment(outState, "currentFragment", fragment);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onChangeDistance(int position) {

        PeopleNearbyFragment p = (PeopleNearbyFragment) fragment;
        p.onChangeDistance(position);
    }

    @Override
    public void onCloseSettingsDialog(int searchGender, int searchOnline) {

        SearchFragment p = (SearchFragment) fragment;
        p.onCloseSettingsDialog(searchGender, searchOnline);
    }

    @Override
    public void onAcceptRequest(int position) {

        NotificationsFragment p = (NotificationsFragment) fragment;
        p.onAcceptRequest(position);
    }

    @Override
    public void onRejectRequest(int position) {

        NotificationsFragment p = (NotificationsFragment) fragment;
        p.onRejectRequest(position);
    }

    @Override
    public void onPhotoFromGallery() {

        ProfileFragment p = (ProfileFragment) fragment;
        p.photoFromGallery();
    }

    @Override
    public void onPhotoFromCamera() {

        ProfileFragment p = (ProfileFragment) fragment;
        p.photoFromCamera();
    }

    @Override
    public void onCoverFromGallery() {

        ProfileFragment p = (ProfileFragment) fragment;
        p.coverFromGallery();
    }

    @Override
    public void onCoverFromCamera() {

        ProfileFragment p = (ProfileFragment) fragment;
        p.coverFromCamera();
    }

    @Override
    public void onProfileReport(int position) {

        ProfileFragment p = (ProfileFragment) fragment;
        p.onProfileReport(position);
    }

    @Override
    public void onProfileBlock() {

        ProfileFragment p = (ProfileFragment) fragment;
        p.onProfileBlock();
    }

    @Override
    public void onPostRePost(int position) {

        switch (page) {

            case 1: {

                ProfileFragment p = (ProfileFragment) fragment;
                p.onPostRePost(position);

                break;
            }

            case 3: {

                FeedFragment p = (FeedFragment) fragment;
                p.onPostRePost(position);

                break;
            }

            case 4: {

                StreamFragment p = (StreamFragment) fragment;
                p.onPostRePost(position);

                break;
            }

            case 5: {

                PopularFragment p = (PopularFragment) fragment;
                p.onPostRePost(position);

                break;
            }

            case 9: {

                FavoritesFragment p = (FavoritesFragment) fragment;
                p.onPostRePost(position);

                break;
            }

            default: {

                break;
            }
        }
    }

    @Override
    public void onPostShare(int position) {

        switch (page) {

            case 1: {

                ProfileFragment p = (ProfileFragment) fragment;
                p.onPostShare(position);

                break;
            }

            case 3: {

                FeedFragment p = (FeedFragment) fragment;
                p.onPostShare(position);

                break;
            }

            case 4: {

                StreamFragment p = (StreamFragment) fragment;
                p.onPostShare(position);

                break;
            }

            case 5: {

                PopularFragment p = (PopularFragment) fragment;
                p.onPostShare(position);

                break;
            }

            case 9: {

                FavoritesFragment p = (FavoritesFragment) fragment;
                p.onPostShare(position);

                break;
            }

            default: {

                break;
            }
        }
    }

    @Override
    public void onPostDelete(int position) {

        switch (page) {

            case 1: {

                ProfileFragment p = (ProfileFragment) fragment;
                p.onPostDelete(position);

                break;
            }

            case 3: {

                FeedFragment p = (FeedFragment) fragment;
                p.onPostDelete(position);

                break;
            }

            case 4: {

                StreamFragment p = (StreamFragment) fragment;
                p.onPostDelete(position);

                break;
            }

            case 5: {

                PopularFragment p = (PopularFragment) fragment;
                p.onPostDelete(position);

                break;
            }

            case 9: {

                FavoritesFragment p = (FavoritesFragment) fragment;
                p.onPostDelete(position);

                break;
            }

            default: {

                break;
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if(CheckInternet.isInternetOn(this)){
            internatePopup.setVisibility(View.GONE);
        }else{
            internatePopup.setVisibility(View.VISIBLE);
        }

        if(showingConnectivity == null){
            showingConnectivity = new showingConnectivity();

        }
        registerReceiver(showingConnectivity,new IntentFilter("Internate_Connection"));

    }

    @Override
    protected void onStart() {

        if(CheckInternet.isInternetOn(this)){
            internatePopup.setVisibility(View.GONE);
        }else{
            internatePopup.setVisibility(View.VISIBLE);
        }

        super.onStart();
    }

    @Override
    protected void onStop() {

        if(showingConnectivity !=null){
           unregisterReceiver(showingConnectivity);
        }
        super.onStop();

    }

    @Override
    public void onPostRemove(int position) {

        switch (page) {

            case 1: {

                ProfileFragment p = (ProfileFragment) fragment;
                p.onPostRemove(position);

                break;
            }

            case 3: {

                FeedFragment p = (FeedFragment) fragment;
                p.onPostRemove(position);

                break;
            }

            case 4: {

                StreamFragment p = (StreamFragment) fragment;
                p.onPostRemove(position);

                break;
            }

            case 5: {

                PopularFragment p = (PopularFragment) fragment;
                p.onPostRemove(position);

                break;
            }

            case 9: {

                FavoritesFragment p = (FavoritesFragment) fragment;
                p.onPostRemove(position);

                break;
            }

            default: {

                break;
            }
        }
    }

    @Override
    public void onPostEdit(int position) {

        switch (page) {

            case 1: {

                ProfileFragment p = (ProfileFragment) fragment;
                p.onPostEdit(position);

                break;
            }

            case 3: {

                FeedFragment p = (FeedFragment) fragment;
                p.onPostEdit(position);

                break;
            }

            case 4: {

                StreamFragment p = (StreamFragment) fragment;
                p.onPostEdit(position);

                break;
            }

            case 5: {

                PopularFragment p = (PopularFragment) fragment;
                p.onPostEdit(position);

                break;
            }

            case 9: {

                FavoritesFragment p = (FavoritesFragment) fragment;
                p.onPostEdit(position);

                break;
            }

            default: {

                break;
            }
        }
    }

    @Override
    public void onPostCopyLink(int position) {

        switch (page) {

            case 1: {

                ProfileFragment p = (ProfileFragment) fragment;
                p.onPostCopyLink(position);

                break;
            }

            case 3: {

                FeedFragment p = (FeedFragment) fragment;
                p.onPostCopyLink(position);

                break;
            }

            case 4: {

                StreamFragment p = (StreamFragment) fragment;
                p.onPostCopyLink(position);

                break;
            }

            case 5: {

                PopularFragment p = (PopularFragment) fragment;
                p.onPostCopyLink(position);

                break;
            }

            case 9: {

                FavoritesFragment p = (FavoritesFragment) fragment;
                p.onPostCopyLink(position);

                break;
            }

            default: {

                break;
            }
        }
    }

    @Override
    public void onPostReportDialog(int position) {

        switch (page) {

            case 1: {

                ProfileFragment p = (ProfileFragment) fragment;
                p.report(position);

                break;
            }

            case 3: {

                FeedFragment p = (FeedFragment) fragment;
                p.report(position);

                break;
            }

            case 4: {

                StreamFragment p = (StreamFragment) fragment;
                p.report(position);

                break;
            }

            case 5: {

                PopularFragment p = (PopularFragment) fragment;
                p.report(position);

                break;
            }

            case 9: {

                FavoritesFragment p = (FavoritesFragment) fragment;
                p.report(position);

                break;
            }

            default: {

                break;
            }
        }
    }

    @Override
    public void onPostReport(int position, int reasonId) {

        switch (page) {

            case 1: {

                ProfileFragment p = (ProfileFragment) fragment;
                p.onPostReport(position, reasonId);

                break;
            }

            case 3: {

                FeedFragment p = (FeedFragment) fragment;
                p.onPostReport(position, reasonId);

                break;
            }

            case 4: {

                StreamFragment p = (StreamFragment) fragment;
                p.onPostReport(position, reasonId);

                break;
            }

            case 5: {

                PopularFragment p = (PopularFragment) fragment;
                p.onPostReport(position, reasonId);

                break;
            }

            case 9: {

                FavoritesFragment p = (FavoritesFragment) fragment;
                p.onPostReport(position, reasonId);

                break;
            }

            default: {

                break;
            }
        }
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {

        displayView(position);
    }

    private void displayView(int position) {

        action = false;

        switch (position) {

            case 0: {

                break;
            }

            case 1: {

                page = 1;

                fragment = new ProfileFragment();

                getSupportActionBar().setTitle(R.string.page_1);

                action = true;

                break;
            }

            case 2: {

                page = 2;

                fragment = new GalleryFragment();
                getSupportActionBar().setTitle(R.string.page_11);

                action = true;

                break;
            }

            case 3: {

                page = 3;

                fragment = new FeedFragment();
                getSupportActionBar().setTitle(R.string.page_2);

                action = true;

                break;
            }

            case 4: {

                page = 4;

                fragment = new StreamFragment();
                getSupportActionBar().setTitle(R.string.page_3);

                action = true;

                break;
            }

            case 5: {

                page = 5;

                fragment = new PopularFragment();
                getSupportActionBar().setTitle(R.string.page_4);

                action = true;

                break;
            }

            case 6: {

                page = 6;

                fragment = new FriendsFragment();
                getSupportActionBar().setTitle(R.string.page_5);

                action = true;

                break;
            }

            case 7: {

                page = 7;

                fragment = new NotificationsFragment();


                action = true;

                break;
            }

            case 8: {

                page = 8;

                fragment = new DialogsFragment();
                getSupportActionBar().setTitle(R.string.page_7);

                action = true;

                break;
            }

            case 9: {

                page = 9;

                fragment = new FavoritesFragment();
                getSupportActionBar().setTitle(R.string.page_8);

                action = true;

                break;
            }

            case 10: {

                page = 10;

                fragment = new GuestsFragment();
                getSupportActionBar().setTitle(R.string.page_12);

                action = true;

                break;
            }

            case 11: {

                page = 11;

                fragment = new GroupsFragment();
                getSupportActionBar().setTitle(R.string.page_9);

                action = true;

                break;
            }

            case 12: {

                page = 12;

                fragment = new UpgradesFragment();
                getSupportActionBar().setTitle(R.string.page_14);

                action = true;

                break;
            }

            case 13: {

                page = 13;

                fragment = new PeopleNearbyFragment();
                getSupportActionBar().setTitle(R.string.page_15);

                action = true;

                break;
            }

            case 14: {

                page = 14;

                fragment = new SearchFragment();
                getSupportActionBar().setTitle("Search");

                action = true;

                break;
            }

            default: {

                Intent i = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(i);
            }
        }

        if (action) {

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container_body, fragment)
                    .commit();
        }
    }

    @Override
    public void onPhotoDelete(int position) {

        GalleryFragment p = (GalleryFragment) fragment;
        p.onPhotoDelete(position);
    }

    @Override
    public void onPhotoRemoveDialog(int position) {

        GalleryFragment p = (GalleryFragment) fragment;
        p.onPhotoRemove(position);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home: {

                return true;
            }

            default: {

                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void onBackPressed() {

        if (drawerFragment.isDrawerOpen()) {

            drawerFragment.closeDrawer();

        } else {

            super.onBackPressed();
        }
    }

    @Override
    public void setTitle(CharSequence title) {

        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    public void hideAds() {

        if (App.getInstance().getAdmob() == ADMOB_DISABLED) {

            mContainerAdmob.setVisibility(View.GONE);
        }
    }

    public  class showingConnectivity extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            boolean isNeternateOn = intent.getBooleanExtra("isInternateOn",false);
            updateUI.OnwhenInternateOn(isNeternateOn);
        }
    }

    @Override
    public void OnwhenInternateOn(boolean ison) {
        if(ison){
            if(minternetlister != null){
                minternetlister.OnSyncDataOfflineToOnline();
            }
            internatePopup.setVisibility(View.GONE);

        }else{
            internatePopup.setVisibility(View.VISIBLE);
        }
    }

    public interface InternetListner{
        void OnInternetOn(boolean isOn);
        void OnSyncDataOfflineToOnline();
    }

}
