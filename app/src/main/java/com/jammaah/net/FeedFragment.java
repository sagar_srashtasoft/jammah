package com.jammaah.net;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.jammaah.net.realm.realmModel.OffLineDataItems;
import com.jammaah.net.realm.realmModel.RealmController;
import com.melnykov.fab.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.jammaah.net.adapter.AdvancedItemListAdapter;
import com.jammaah.net.app.App;
import com.jammaah.net.constants.Constants;
import com.jammaah.net.dialogs.MyPostActionDialog;
import com.jammaah.net.dialogs.PostActionDialog;
import com.jammaah.net.dialogs.PostDeleteDialog;
import com.jammaah.net.dialogs.PostReportDialog;
import com.jammaah.net.dialogs.PostShareDialog;
import com.jammaah.net.model.Item;
import com.jammaah.net.util.Api;
import com.jammaah.net.util.CustomRequest;
import com.jammaah.net.util.ItemInterface;



import io.realm.Realm;
import io.realm.RealmResults;

public class FeedFragment extends Fragment implements Constants, SwipeRefreshLayout.OnRefreshListener, ItemInterface,MainActivity.InternetListner,AdvancedItemListAdapter.TaptoReyry {

    private static final String STATE_LIST = "State Adapter Data";

    private static final int PROFILE_NEW_POST = 4;

    RecyclerView mRecyclerView;
    TextView mMessage,tv_no_internet_text;

    SwipeRefreshLayout mItemsContainer;
    private Realm realm;

    FloatingActionButton mFabButton;

    private ArrayList<Item> itemsList;
    private AdvancedItemListAdapter itemsAdapter;

    private int itemId = 0;
    private int arrayLength = 0;
    private Boolean loadingMore = false;
    private Boolean viewMore = false;
    private Boolean restore = false;
    boolean offlineData = false;
    ArrayList<OffLineDataItems> itemsList1 = new ArrayList<>();

    int pastVisiblesItems = 0, visibleItemCount = 0, totalItemCount = 0;

    public FeedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {

            itemsList = savedInstanceState.getParcelableArrayList(STATE_LIST);
            itemsAdapter = new AdvancedItemListAdapter(getActivity(), itemsList);
            itemsAdapter.mTaptoReyry = FeedFragment.this;
            restore = savedInstanceState.getBoolean("restore");
            itemId = savedInstanceState.getInt("itemId");

        } else {

            itemsList = new ArrayList<>();
            itemsAdapter = new AdvancedItemListAdapter(getActivity(), itemsList);
            itemsAdapter.mTaptoReyry = FeedFragment.this;
            restore = false;
            itemId = 0;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_feed, container, false);

        mItemsContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.container_items);
        mItemsContainer.setOnRefreshListener(this);

        mMessage = (TextView) rootView.findViewById(R.id.message);
        tv_no_internet_text = (TextView) rootView.findViewById(R.id.tv_no_internet_text);


        mFabButton = (FloatingActionButton) rootView.findViewById(R.id.fabButton);
        mFabButton.setImageResource(R.drawable.ic_action_new);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        this.realm = RealmController.with(this).getRealm();

        if(!App.getInstance().isConnected()){
           offlineData = true;
            setRealmAdapter(RealmController.with(this).getOffLineItem());
            MainActivity.minternetlister = this;

        }

        itemsAdapter.setOnMoreButtonClickListener(new AdvancedItemListAdapter.OnItemMenuButtonClickListener() {

            @Override
            public void onItemClick(View v, Item obj, int actionId, int position) {

                switch (actionId) {

                    case R.id.action_repost: {

                        if (obj.getFromUserId() != App.getInstance().getId()) {

                            if (obj.getRePostFromUserId() != App.getInstance().getId()) {

                                repost(position);

                            } else {

                                Toast.makeText(getActivity(), getActivity().getString(R.string.msg_not_make_repost), Toast.LENGTH_SHORT).show();
                            }

                        } else {

                            Toast.makeText(getActivity(), getActivity().getString(R.string.msg_not_make_repost), Toast.LENGTH_SHORT).show();
                        }

                        break;
                    }

                    case R.id.action_share: {

                        onPostShare(position);

                        break;
                    }

                    case R.id.action_report: {

                        report(position);

                        break;
                    }

                    case R.id.action_remove: {

                        remove(position);

                        break;
                    }
                }
            }
        });

        final GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);

        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.setAdapter(itemsAdapter);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                if(dy > 0) {

                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (!loadingMore) {

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount && (viewMore) && !(mItemsContainer.isRefreshing())) {

                            loadingMore = true;
                            Log.e("...", "Last Item Wow !");
                            if(App.getInstance().isConnected()){
                               getItems();
                              /*  Item item = new Item();
                                item.setAd(1);
                                item.setCity("Noiternate");
                                // itemsList.add(item);
                                itemsAdapter.addNo_Internate(item);*/
                            }else{
                                Item item = new Item();
                                item.setAd(1);
                                item.setCity("Noiternate");
                               // itemsList.add(item);
                                itemsAdapter.addNo_Internate(item);
                            }


                        }
                    }
                }
            }
        });
        mFabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), NewItemActivity.class);
                startActivityForResult(intent, FEED_NEW_POST);
            }
        });

        if (itemsAdapter.getItemCount() == 0) {

            showMessage(getText(R.string.label_empty_list).toString());

        } else {

            hideMessage();
        }

        if (!restore) {

            showMessage(getText(R.string.msg_loading_2).toString());

            getItems();
        }

        return rootView;
    }
public void setRealmAdapter(RealmResults<OffLineDataItems> offLineDataItemses){
    itemsList = new ArrayList<>();
    if(offLineDataItemses.size() > 0) {
        //offLineDataItemses.get(0).getCity();
        for (int i = 0; i < offLineDataItemses.size(); i++) {
            try {
                Item item = new Item();
                item.setId(offLineDataItemses.get(i).getId());
                item.setRePostId(offLineDataItemses.get(i).getRePostId());
                item.setGroupId(offLineDataItemses.get(i).getGroupId());
                item.setFromUserId(offLineDataItemses.get(i).getFromUserId());
                item.setAccessMode(offLineDataItemses.get(i).getAccessMode());
                item.setFromUserVerify(offLineDataItemses.get(i).getAccessMode());
                item.setFromUserUsername(offLineDataItemses.get(i).getFromUserUsername());
                item.setFromUserFullname(offLineDataItemses.get(i).getFromUserFullname());
                item.setFromUserPhotoUrl(offLineDataItemses.get(i).getFromUserPhotoUrl());
                item.setPost(offLineDataItemses.get(i).getPost());
                item.setImgUrl(offLineDataItemses.get(i).getImgUrl());
                item.setArea(offLineDataItemses.get(i).getArea());
                item.setCountry(offLineDataItemses.get(i).getCountry());
                item.setCity(offLineDataItemses.get(i).getCity());
                item.setAllowComments(offLineDataItemses.get(i).getAllowComments());
                item.setGroupAllowComments(offLineDataItemses.get(i).getGroupAllowComments());
                item.setGroupAuthor(offLineDataItemses.get(i).getGroupAuthor());
                item.setCommentsCount(offLineDataItemses.get(i).getCommentsCount());
                item.setLikesCount(offLineDataItemses.get(i).getLikesCount());
                item.setRePostsCount(offLineDataItemses.get(i).getRePostsCount());
                item.setMyLike(false);
                item.setRePostPost(offLineDataItemses.get(i).getRePostPost());
                item.setMyRePost(false);
                item.setLat(0.000000);
                item.setLng(0.000000);
                item.setRePostImgUrl(offLineDataItemses.get(i).getRePostImgUrl());
                item.setDate(offLineDataItemses.get(i).getDate());
                item.setTimeAgo(offLineDataItemses.get(i).getTimeAgo());


                item.setYouTubeVideoImg(offLineDataItemses.get(i).getYouTubeVideoImg());
                item.setYouTubeVideoCode(offLineDataItemses.get(i).getYouTubeVideoCode());
                item.setYouTubeVideoUrl(offLineDataItemses.get(i).getYouTubeVideoUrl());

                item.setUrlPreviewTitle(offLineDataItemses.get(i).getUrlPreviewTitle());
                item.setUrlPreviewImage(offLineDataItemses.get(i).getUrlPreviewImage());
                item.setUrlPreviewLink(offLineDataItemses.get(i).getUrlPreviewLink());
                item.setUrlPreviewDescription(offLineDataItemses.get(i).getUrlPreviewDescription());

                item.setVideoUrl(offLineDataItemses.get(i).getVideoUrl());
                item.setPreviewVideoImgUrl(offLineDataItemses.get(i).getPreviewVideoImgUrl());
                itemsList.add(item);

                // json = (org.json.simple.JSONObject) jsonParser.parse(offLineDataItemses.get(i).getStringJson());
                // Item item = new Item(json);
                // itemsList.add(item);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
        itemsAdapter = new AdvancedItemListAdapter(getActivity(), itemsList);
    itemsAdapter.mTaptoReyry = FeedFragment.this;
        mRecyclerView.setAdapter(itemsAdapter);

    Item item = new Item();
    item.setAd(1);
    item.setCity("Noiternate");
    // itemsList.add(item);
    itemsAdapter.addNo_Internate(item);


}

    @Override
    public void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);

        outState.putBoolean("restore", true);
        outState.putInt("itemId", itemId);
        outState.putParcelableArrayList(STATE_LIST, itemsList);
    }

    @Override
    public void onRefresh() {

        if (App.getInstance().isConnected()) {

            itemId = 0;
            getItems();

        } else {

            mItemsContainer.setRefreshing(false);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == FEED_NEW_POST && resultCode == getActivity().RESULT_OK && null != data) {

            itemId = 0;
            getItems();

        } else if (requestCode == ITEM_EDIT && resultCode == getActivity().RESULT_OK) {

            int position = data.getIntExtra("position", 0);

            Item item = itemsList.get(position);

            item.setPost(data.getStringExtra("post"));
            item.setImgUrl(data.getStringExtra("imgUrl"));

            itemsAdapter.notifyDataSetChanged();

        } else if (requestCode == ITEM_REPOST && resultCode == getActivity().RESULT_OK) {

            int position = data.getIntExtra("position", 0);

            Item item = itemsList.get(position);

            item.setMyRePost(true);
            item.setRePostsCount(item.getRePostsCount() + 1);

            itemsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void getItems() {



        mItemsContainer.setRefreshing(true);

        CustomRequest jsonReq = new CustomRequest(Request.Method.POST, METHOD_FEEDS_GET, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        if (!isAdded() || getActivity() == null) {

                            Log.e("ERROR", "FeedFragment Not Added to Activity");

                            return;
                        }

                        if (!loadingMore) {
                            RealmController.with(getActivity()).clearAllPost();
                            itemsList.clear();

                        }

                        try {

                            arrayLength = 0;

                            if (!response.getBoolean("error")) {

                                itemId = response.getInt("itemId");

                                if (response.has("items")) {

                                    JSONArray itemsArray = response.getJSONArray("items");

                                    arrayLength = itemsArray.length();

                                    if (arrayLength > 0) {

                                        for (int i = 0; i < itemsArray.length(); i++) {

                                            JSONObject itemObj = (JSONObject) itemsArray.get(i);

                                            Item item = new Item(itemObj);
                                            setRealmData(itemObj);

                                            item.setAd(0);

                                            itemsList.add(item);

                                            // Ad after first item
                                            if (i == MY_AD_AFTER_ITEM_NUMBER && App.getInstance().getAdmob() == ENABLED) {

                                                Item ad = new Item(itemObj);

                                                ad.setAd(1);

                                                itemsList.add(ad);
                                            }
                                        }
                                    }
                                }
                            }

                        } catch (JSONException e) {

                            e.printStackTrace();

                        } finally {

                            loadingComplete();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (!isAdded() || getActivity() == null) {

                    Log.e("ERROR", "FeedFragment Not Added to Activity");

                    return;
                }

                loadingComplete();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("accountId", Long.toString(App.getInstance().getId()));
                params.put("accessToken", App.getInstance().getAccessToken());
                params.put("itemId", Integer.toString(itemId));
                params.put("language", "en");

                return params;
            }
        };

        App.getInstance().addToRequestQueue(jsonReq);
    }

    public void setRealmData(JSONObject jsonObject){
        OffLineDataItems offLineDataItems = new OffLineDataItems(jsonObject);
       offLineDataItems.setStringJson(jsonObject.toString());
        //offLineDataItems.setItem(item);

    // offLineDataItems.setAd();
       //itemsList1.add(offLineDataItems);
        realm.beginTransaction();;
        realm.copyToRealm(offLineDataItems);
        realm.commitTransaction();
    }


    public void loadingComplete() {

        if (arrayLength == LIST_ITEMS) {

            viewMore = true;

        } else {

            viewMore = false;
        }

        itemsAdapter.notifyDataSetChanged();

        if (itemsAdapter.getItemCount() == 0) {

            if (FeedFragment.this.isVisible()) {

                showMessage(getText(R.string.label_empty_list).toString());
            }

        } else {

            hideMessage();
        }

        loadingMore = false;
        mItemsContainer.setRefreshing(false);
    }

    public void report(int position) {

        android.app.FragmentManager fm = getActivity().getFragmentManager();

        PostReportDialog alert = new PostReportDialog();

        Bundle b  = new Bundle();
        b.putInt("position", position);
        b.putInt("reason", 0);

        alert.setArguments(b);
        alert.show(fm, "alert_dialog_post_report");
    }

    public void onPostReport(int position, int reasonId) {

        final Item item = itemsList.get(position);

        if (App.getInstance().isConnected()) {

            Api api = new Api(getActivity());

            api.postReport(item.getId(), reasonId);

        } else {

            Toast.makeText(getActivity(), getText(R.string.msg_network_error), Toast.LENGTH_SHORT).show();
        }
    }

    public void remove(int position) {

        android.app.FragmentManager fm = getActivity().getFragmentManager();

        PostDeleteDialog alert = new PostDeleteDialog();

        Bundle b  = new Bundle();
        b.putInt("position", position);

        alert.setArguments(b);
        alert.show(fm, "alert_dialog_post_delete");
    }

    public void onPostDelete(int position) {

        final Item item = itemsList.get(position);

        itemsList.remove(position);
        itemsAdapter.notifyDataSetChanged();

        if (itemsAdapter.getItemCount() == 0) {

            showMessage(getText(R.string.label_empty_list).toString());

        } else {

            hideMessage();
        }

        if (App.getInstance().isConnected()) {

            Api api = new Api(getActivity());

            api.postDelete(item.getId());

        } else {

            Toast.makeText(getActivity(), getText(R.string.msg_network_error), Toast.LENGTH_SHORT).show();
        }
    }

    public void onPostRemove(final int position) {

        /** Getting the fragment manager */
        android.app.FragmentManager fm = getActivity().getFragmentManager();

        /** Instantiating the DialogFragment class */
        PostDeleteDialog alert = new PostDeleteDialog();

        /** Creating a bundle object to store the selected item's index */
        Bundle b  = new Bundle();

        /** Storing the selected item's index in the bundle object */
        b.putInt("position", position);

        /** Setting the bundle object to the dialog fragment object */
        alert.setArguments(b);

        /** Creating the dialog fragment object, which will in turn open the alert dialog window */

        alert.show(fm, "alert_dialog_post_delete");
    }

    public void onPostShare(final int position) {

        final Item item = itemsList.get(position);

        Api api = new Api(getActivity());
        api.postShare(item);
    }

    public void onPostEdit(final int position) {

        Item item = itemsList.get(position);

        Intent i = new Intent(getActivity(), EditItemActivity.class);
        i.putExtra("position", position);
        i.putExtra("postId", item.getId());
        i.putExtra("post", item.getPost());
        i.putExtra("imgUrl", item.getImgUrl());
        startActivityForResult(i, ITEM_EDIT);
    }

    public void onPostCopyLink(final int position) {

        final Item item = itemsList.get(position);

        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(getActivity().CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("post url", item.getLink());
        clipboard.setPrimaryClip(clip);

        Toast.makeText(getActivity(), getText(R.string.msg_post_link_copied), Toast.LENGTH_SHORT).show();
    }

    public void action(int position) {

        final Item item = itemsList.get(position);

        if (item.getFromUserId() == App.getInstance().getId()) {

            /** Getting the fragment manager */
            android.app.FragmentManager fm = getActivity().getFragmentManager();

            /** Instantiating the DialogFragment class */
            MyPostActionDialog alert = new MyPostActionDialog();

            /** Creating a bundle object to store the selected item's index */
            Bundle b  = new Bundle();

            /** Storing the selected item's index in the bundle object */
            b.putInt("position", position);

            /** Setting the bundle object to the dialog fragment object */
            alert.setArguments(b);

            /** Creating the dialog fragment object, which will in turn open the alert dialog window */

            alert.show(fm, "alert_my_post_action");

        } else {

            /** Getting the fragment manager */
            android.app.FragmentManager fm = getActivity().getFragmentManager();

            /** Instantiating the DialogFragment class */
            PostActionDialog alert = new PostActionDialog();

            /** Creating a bundle object to store the selected item's index */
            Bundle b  = new Bundle();

            /** Storing the selected item's index in the bundle object */
            b.putInt("position", position);

            /** Setting the bundle object to the dialog fragment object */
            alert.setArguments(b);

            /** Creating the dialog fragment object, which will in turn open the alert dialog window */

            alert.show(fm, "alert_post_action");
        }
    }

    public void repost(int position) {

        android.app.FragmentManager fm = getActivity().getFragmentManager();

        PostShareDialog alert = new PostShareDialog();

        Bundle b  = new Bundle();

        b.putInt("position", position);

        alert.setArguments(b);

        alert.show(fm, "alert_repost_action");
    }

    public void onPostRePost(final int position) {

        Item item = itemsList.get(position);

        long rePostId = item.getId();

        if (item.getRePostId() != 0) {

            rePostId = item.getRePostId();
        }

        Intent i = new Intent(getActivity(), RePostItemActivity.class);
        i.putExtra("position", position);
        i.putExtra("rePostId", rePostId);
        startActivityForResult(i, ITEM_REPOST);
    }

    public void showMessage(String message) {

        mMessage.setText(message);
        mMessage.setVisibility(View.VISIBLE);
    }

    public void hideMessage() {

        mMessage.setVisibility(View.GONE);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void OnInternetOn(boolean isOn) {

    }

    @Override
    public void OnSyncDataOfflineToOnline() {
        if(offlineData){
            itemId = 0;
            offlineData = false;
            getItems();
        }
    }

    @Override
    public void OnReryListner() {
        getItems();
    }
}