package com.jammaah.net;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.jammaah.net.adapter.DialogListAdapter;
import com.jammaah.net.app.App;
import com.jammaah.net.constants.Constants;
import com.jammaah.net.model.Chat;
import com.jammaah.net.realm.realmModel.OfflineMessages;
import com.jammaah.net.realm.realmModel.RealmController;
import com.jammaah.net.util.CustomRequest;

import io.realm.Realm;
import io.realm.RealmResults;

public class DialogsFragment extends Fragment implements Constants, SwipeRefreshLayout.OnRefreshListener {

    private static final String STATE_LIST = "State Adapter Data";

    ListView mListView;
    TextView mMessage;
    ImageView mSplash,no_internate;
    private Realm realm;;

    SwipeRefreshLayout mItemsContainer;

    private ArrayList<Chat> chatsList;
    private DialogListAdapter chatsAdapter;

    private int messageCreateAt = 0;
    private int arrayLength = 0;
    private Boolean loadingMore = false;
    private Boolean viewMore = false;
    private Boolean restore = false;

    public DialogsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {

            chatsList = savedInstanceState.getParcelableArrayList(STATE_LIST);
            chatsAdapter = new DialogListAdapter(getActivity(), chatsList);

            restore = savedInstanceState.getBoolean("restore");
            messageCreateAt = savedInstanceState.getInt("messageCreateAt");

        } else {

            chatsList = new ArrayList<Chat>();
            chatsAdapter = new DialogListAdapter(getActivity(), chatsList);

            restore = false;
            messageCreateAt = 0;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_dialogs, container, false);

        mItemsContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.container_items);
        mItemsContainer.setOnRefreshListener(this);

        mMessage = (TextView) rootView.findViewById(R.id.message);
        mSplash = (ImageView) rootView.findViewById(R.id.splash);
        no_internate = (ImageView) rootView.findViewById(R.id.no_internate);

        mListView = (ListView) rootView.findViewById(R.id.listView);
        mListView.setAdapter(chatsAdapter);

        this.realm = RealmController.with(this).getRealm();


        if (chatsAdapter.getCount() == 0) {

            showMessage(getText(R.string.label_empty_list).toString());

        } else {

            hideMessage();
        }

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                Chat chat = (Chat) adapterView.getItemAtPosition(position);

                Intent intent = new Intent(getActivity(), ChatActivity.class);
                intent.putExtra("position", position);
                intent.putExtra("chatId", chat.getId());
                intent.putExtra("profileId", chat.getWithUserId());
                intent.putExtra("withProfile", chat.getWithUserFullname());

                intent.putExtra("blocked", chat.getBlocked());

                intent.putExtra("fromUserId", chat.getFromUserId());
                intent.putExtra("toUserId", chat.getToUserId());

                startActivityForResult(intent, VIEW_CHAT);

                chat.setNewMessagesCount(0);

                if (App.getInstance().getMessagesCount() > 0) {

                    App.getInstance().setMessagesCount(App.getInstance().getMessagesCount() - 1);
                }

                chatsAdapter.notifyDataSetChanged();
            }
        });

        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                int lastInScreen = firstVisibleItem + visibleItemCount;

                if ((lastInScreen == totalItemCount) && !(loadingMore) && (viewMore) && !(mItemsContainer.isRefreshing())) {

                    if (App.getInstance().isConnected()) {

                        loadingMore = true;

                        getConversations();
                    }
                }
            }
        });

        if (!restore) {
            if(!App.getInstance().isConnected()){

                setRealmAdapter(RealmController.with(this).getOfflineMessages());



            }else{
                no_internate.setVisibility(View.GONE);
                showMessage(getText(R.string.msg_loading_2).toString());

                getConversations();
            }


        }


        // Inflate the layout for this fragment
        return rootView;
    }

    public void setRealmAdapter(RealmResults<OfflineMessages> offlineMessages) {
    chatsList = new ArrayList<>();
        if(offlineMessages.size() > 0){

            for(int i =0; i< offlineMessages.size() ; i++){
               Chat chat = new Chat();
                chat.setId(offlineMessages.get(i).getId());
                chat.setFromUserId(offlineMessages.get(i).getFromUserId());
                chat.setToUserId(offlineMessages.get(i).getToUserId());
                chat.setWithUserId(offlineMessages.get(i).getWithUserId());
                chat.setWithUserVerify(offlineMessages.get(i).getWithUserVerify());
                chat.setWithUserState(offlineMessages.get(i).getWithUserState());
                 chat.setWithUserUsername(offlineMessages.get(i).getWithUserUsername());
                chat.setWithUserFullname(offlineMessages.get(i).getWithUserFullname());
                chat.setWithUserPhotoUrl(offlineMessages.get(i).getWithUserPhotoUrl());
                chat.setLastMessage(offlineMessages.get(i).getLastMessage());
                chat.setLastMessageAgo(offlineMessages.get(i).getLastMessageAgo());
                chat.setNewMessagesCount(offlineMessages.get(i).getNewMessagesCount());
                chat.setDate(offlineMessages.get(i).getDate());
                chat.setCreateAt(offlineMessages.get(i).getCreateAt());
                chat.setTimeAgo(offlineMessages.get(i).getTimeAgo());
                chatsList.add(chat);

            }
            chatsAdapter = new DialogListAdapter(getActivity(), chatsList);
            mListView.setAdapter(chatsAdapter);


            mSplash.setVisibility(View.GONE);
            mMessage.setText("");

        }else{
            no_internate.setVisibility(View.VISIBLE);
            mSplash.setVisibility(View.GONE);
            mMessage.setText("");
        }

    }

    public void setRealmData(JSONObject jsonObject){
        OfflineMessages offLineFriendsList = new OfflineMessages(jsonObject);
        // offLineDataItems.setStringJson(jsonObject.toString());
        //offLineDataItems.setItem(item);

        // offLineDataItems.setAd();
        //itemsList1.add(offLineDataItems);
        realm.beginTransaction();;
        realm.copyToRealm(offLineFriendsList);
        realm.commitTransaction();
    }
        @Override
    public void onRefresh() {

        if (App.getInstance().isConnected()) {

            messageCreateAt = 0;
            getConversations();
            no_internate.setVisibility(View.GONE);

        } else {

            no_internate.setVisibility(View.VISIBLE);
            mSplash.setVisibility(View.GONE);
            mMessage.setText("");
            mItemsContainer.setRefreshing(false);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == VIEW_CHAT && resultCode == getActivity().RESULT_OK && null != data) {

            int pos = data.getIntExtra("position", 0);

            Toast.makeText(getActivity(), getString(R.string.msg_chat_has_been_removed), Toast.LENGTH_SHORT).show();

            chatsList.remove(pos);

            chatsAdapter.notifyDataSetChanged();

            if (chatsAdapter.getCount() == 0) {

                showMessage(getText(R.string.label_empty_list).toString());

            } else {

                hideMessage();
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);

        outState.putBoolean("restore", true);
        outState.putInt("messageCreateAt", messageCreateAt);
        outState.putParcelableArrayList(STATE_LIST, chatsList);
    }

    public void getConversations() {

        mItemsContainer.setRefreshing(true);

        CustomRequest jsonReq = new CustomRequest(Request.Method.POST, METHOD_DIALOGS_NEW_GET, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        if (!isAdded() || getActivity() == null) {

                            Log.e("ERROR", "DialogsFragment Not Added to Activity");

                            return;
                        }

                        try {

                            arrayLength = 0;

                            if (!loadingMore) {

                                RealmController.with(getActivity()).clearAllMessagesData();


                                chatsList.clear();
                            }

                            if (!response.getBoolean("error")) {

                                messageCreateAt = response.getInt("messageCreateAt");

                                if (response.has("chats")) {

                                    JSONArray chatsArray = response.getJSONArray("chats");

                                    arrayLength = chatsArray.length();

                                    if (arrayLength > 0) {

                                        for (int i = 0; i < chatsArray.length(); i++) {

                                            JSONObject chatObj = (JSONObject) chatsArray.get(i);

                                            Chat chat = new Chat(chatObj);
                                            setRealmData(chatObj);

                                            chatsList.add(chat);

                                        }
                                    }
                                }
                            }

                        } catch (JSONException e) {

                            loadingComplete();

                            e.printStackTrace();

                        } finally {

                            loadingComplete();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (!isAdded() || getActivity() == null) {

                    Log.e("ERROR", "DialogsFragment Not Added to Activity");

                    return;
                }

                loadingComplete();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("accountId", Long.toString(App.getInstance().getId()));
                params.put("accessToken", App.getInstance().getAccessToken());
                params.put("messageCreateAt", Integer.toString(messageCreateAt));
                params.put("language", "en");

                return params;
            }
        };

        App.getInstance().addToRequestQueue(jsonReq);
    }

    public void loadingComplete() {

        if (arrayLength == LIST_ITEMS) {

            viewMore = true;

        } else {

            viewMore = false;
        }

        chatsAdapter.notifyDataSetChanged();

        if (chatsAdapter.getCount() == 0) {

            showMessage(getText(R.string.label_empty_list).toString());

        } else {

            hideMessage();
        }

        loadingMore = false;
        mItemsContainer.setRefreshing(false);
    }

    public void showMessage(String message) {

        mMessage.setText(message);
        mMessage.setVisibility(View.VISIBLE);

        mSplash.setVisibility(View.VISIBLE);
    }

    public void hideMessage() {

        mMessage.setVisibility(View.GONE);

        mSplash.setVisibility(View.GONE);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}